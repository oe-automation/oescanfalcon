import os       # Used to enable coloring
import sys      # imported to include paths
import glob     # Glob for files
from pathlib import Path

# Color Settings
class ColorValues:
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    CYAN = '\033[96m'
    RESET = '\033[0m'
    UNDERLINE = '\033[4m'

# Import path values
def init_env_path() :
    """ 
        Call this to inclue supporting libs for automation testing.
        This is used when debugging python code that is crashing.
        
        SAMPLE IMPORT CALL

            # SETUP PATH VALUES FOR ALL FOLLOWING CALLS
            import sys
            sys.path.append("../") 

            # RUN THIS IMPORTING FUNCTION
            import FalconEnvSetup
            FalconEnvSetup.init_env_path()

        Once this is run, all values from __path_values__ are included in pythonpath for this execution session
    """

    # Import these paths now.
    os.system("color")
    print (f"\n{ColorValues.CYAN}{ColorValues.UNDERLINE}APPENDING PATH VALUES...{ColorValues.RESET}")
    
    # GEt paths
    path_value = Path(os.getcwd())
    path_start = path_value.parent.parent

    for path in  path_start.glob("**/*"):
        path_relative = os.path.relpath(path, path_start)
        print (f"{ColorValues.YELLOW}--> APPENDING PATH: {path_relative}{ColorValues.RESET}")
        sys.path.append(path_relative)
        
    # Print done
    print (f"\n{ColorValues.GREEN}DONE ADDING PATHS{ColorValues.RESET}")
    print (f"{ColorValues.UNDERLINE}")
    print (f"{ColorValues.RESET}")
