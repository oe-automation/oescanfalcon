###############################################################################################################
#                                               IMPORTS AND LIBS                                              #
###############################################################################################################
import io                        # Filesystem operations
import os                        # Operations for OS 
import FalconMainVariables       # Main Variable object
from FalconLoggerInit import FalconLoggerInit 

###############################################################################################################
#                                              CLASS DEFINITION                                               #
###############################################################################################################
class FalconControlStore:
    """
        +----------------------------------------------------------------------------------------------------+
        | Description:                                                                                       |
        |     Contains a set of controls for a falcon app instance. There should be a set of these objects   |
        |     created for each scan instance. One control set should exist for each page/view of the scan    |
        |     These Control sets will be stored in a tuple/list of a string and this object to allow us to   |
        |     store the entire app as a json configuration file.                                             |
        |                                                                                                    |
        +----------------------------------------------------------------------------------------------------+
        | Methods From FalconControlStore:                                                                   |
        |                                                                                                    |   
        |                                                                                                    |
        |                                                                                                    |
        |                                                                                                    |
        +----------------------------------------------------------------------------------------------------+    
    """

    ###########################################################################################################
    #                                           CLASS VARIABLES                                               #
    ###########################################################################################################

    APP_NAME = None
    FALCON_CONTROLS = None
    FALCON_LOGGER = None

    ###########################################################################################################
    #                                           CLASS FUNCTIONS                                               #
    ###########################################################################################################
    
    def __init__(self, app_name):
        """ 
            Configures a new FalconControlStore
            Pass in the app name, and this method will auto locate the window and pull controls out of it
            storing them in this class object 
        """

        # Get a logger
        self.APP_NAME = app_name
        self.FALCON_LOGGER = FalconLogger.init_logger(app_name)
