###############################################################################################################
#                                               IMPORTS AND LIBS                                              #
###############################################################################################################
import json                          # Used for JSON Reading
from types import SimpleNamespace    # Convering JSON into objects
import FalconMainVariables           # Used for environment configuration
from FalconLogger import FalconLogger

###############################################################################################################
#                                              CLASS DEFINITION                                               #
###############################################################################################################
class FalconJsonConfig:
    """
        ----------------------------------------------------------------------------------------------------
        Description: 
            Class for reading and importing a base scan config file and scan specific JSON Settings.
            Used in the setup method to build us new app settings.
        ----------------------------------------------------------------------------------------------------
        Methods From FalconJsonConfig: 
            --> import_base_config()                Reads in a basic json config file and converts the values 
                                                    into an object
            --> import_scan_config(scan_type)       Reads a scan config file in based on the name of the scan 
                                                    being done 
        ----------------------------------------------------------------------------------------------------
    """

    ###########################################################################################################
    #                                           CLASS VARIABLES                                               #
    ###########################################################################################################

    # Config File Main Path
    CONFIG_PATH_BASE = FalconMainVariables.SCAN_CONFIGS_PATH
    MAIN_CONFIG_FILE = CONFIG_PATH_BASE + "FalconScanConfig.json"

    # Configuration Objeects
    FALCON_CONFIG_MAIN = None
    SCAN_SESSION_CONFIG = None

    # Logging Object
    FALCON_LOGGER = None

    ###########################################################################################################
    #                                           CLASS FUNCTIONS                                               #
    ###########################################################################################################

    def __init__(self):
        """ Generate our logging object """
        self.FALCON_LOGGER = FalconLogger("FalconJsonConfig")
        
    def import_base_config(self) : 
        """
            Reads a JSON Input File Path and stores the configuration of it.
            Store the main config object onto the class instance.
        """

        # Read file input
        BaseConfig = open(self.MAIN_CONFIG_FILE, "r")
        FileContent = BaseConfig.read()
        BaseConfig.close()
    
        # Log Info/values
        self.FALCON_LOGGER.log_message(f"MAIN CONFIG FILE CONTENTS:\n {FileContent}")

        # Convert the contents into an object and store it on the main config
        self.MAIN_CONFIG_FILE = json.loads(FileContent, object_hook=lambda d: SimpleNamespace(**d))
        self.FALCON_LOGGER.log_message("READ IN MAIN CONFIG FILE AND CONVERTED TO A JSON OBJECT CORRECTLY!")
        return self.FALCON_CONFIG_MAIN


    def import_scan_config(self, scan_type) :
        """
            Reads a JSON Input File Path and stores the configuration of it.
            Store the scan session type config object onto the class instance.
        """

        # Build path and read file.
        self.SCAN_CONFIG_PATH = self.CONFIG_PATH_BASE + "Falcon" + scan_type + "Config.json"
        ScanSessionConfig = open(self.SCAN_CONFIG_PATH, "r")
        FileContent = ScanSessionConfig.read()
        ScanSessionConfig.close()
        self.FALCON_LOGGER.log_message(f"FOUND SCAN SESSION LOGGER FOR TYPE {scan_type} CORRECTLY!")
        self.FALCON_LOGGER.log_message(f"FILE CONTENTS:\n {FileContent}")

        # Convert the contents into an object and store it on the main config
        self.SCAN_SESSION_CONFIG = json.loads(FileContent, object_hook=lambda d: SimpleNamespace(**d))
        self.FALCON_LOGGER.log_message("READ IN SESSION CONFIG FILE AND CONVERTED TO A JSON OBJECT CORRECTLY!")
        return self.SCAN_SESSION_CONFIG