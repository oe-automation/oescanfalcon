###############################################################################################################
#                                               IMPORTS AND LIBS                                              #
###############################################################################################################
import io                        # Filesystem operations
import os                        # Operations for OS 
import FalconMainVariables       # Used for environment configuration

###############################################################################################################
#                                              CLASS DEFINITION                                               #
###############################################################################################################
class LaunchOEApp:
    """ 
        +----------------------------------------------------------------------------------------------------+
        | Description:                                                                                       |
        |     Launches the OE app in the main scan configuration and stores the controls of it               |
        |     This is reserved really for the statup classes/robots but I guess it can be invoked anywhere   |
        |     If ya feelin lucky, punk                                                                       |
        |                                                                                                    |
        +----------------------------------------------------------------------------------------------------+
        | Methods From LaunchOEApp:                                                                          |
        |     --> start_app()                   Launches the OE Scan App located in the app setup config     |
        |                                                                                                    |
        |     --> get_app_controls()            Takes the launched application and confirues the controls of | 
        |                                       it. Stored in the class by default but can be exported.      |
        +----------------------------------------------------------------------------------------------------+     
     """

    ###########################################################################################################
    #                                           CLASS FUNCTIONS                                               #
    ###########################################################################################################

    def start_app(self) : 
        """
            Launches the OE ApplicationLaunches the OE Scan App located in the app setup config
            Stores the app instance on the main class.
        """


    def get_app_controls(self) : 
        """ 
            Takes the launched application and confirues the controls of it. 
            Stored in the class by default but can be exported.     
        """
