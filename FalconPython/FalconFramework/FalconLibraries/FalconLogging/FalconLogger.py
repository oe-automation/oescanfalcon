###############################################################################################################
#                                               IMPORTS AND LIBS                                              #
###############################################################################################################
import logging
import FalconMainVariables                       # Main Variable object
from FalconLoggerInit import FalconLoggerInit    # Logger Setup

###############################################################################################################
#                                              CLASS DEFINITION                                               #
###############################################################################################################
class FalconLogger:
    """
        +----------------------------------------------------------------------------------------------------+
        | Description:                                                                                       |
        |     Color objects for coloring console output. This is replaced by colorama once this script       |
        |     is done running and all needed modules have been pulled in. But since we're assuming this      |
        |     is to be a base install, we just use the color defs here                                       |
        +----------------------------------------------------------------------------------------------------+
        | Methods From FalconLogger:                                                                         |
        |     --> init_new_logger(logger_name, filename)       Builds a new logger with the name and path    |
        |                                                      value given.                                  |
        |                                                      Returns the logger generated                  |
        |                                                      If a logger is defined when this method is    |
        |                                                      called, the existing one is given back.       |
        |                                                                                                    |
        +----------------------------------------------------------------------------------------------------+    
    """

    ###########################################################################################################
    #                                           CLASS FUNCTIONS                                               #
    ###########################################################################################################

    LOGGER_NAME = None        
    def __init__(self, logger_name = None):
        if (logger_name == None): return

        # Setup Logger
        self.LOGGER_NAME = logger_name
        self.init_logger(self.LOGGER_NAME)

    def log_message(self, message, level="INFO", logger_name = None) :
        """ Writes a log message out with the given info level """

        # Check Logger Name
        if (logger_name == None):
            if (self.LOGGER_NAME != None): logger_name = self.LOGGER_NAME    
            else: 
                self.LOGGER_NAME = str(__name__)
                logger_name = self.LOGGER_NAME

        # pull logger 
        logger = self.init_logger(self.LOGGER_NAME)

        # Get the log level and write it out 
        log_level = getattr(logging, level.upper())
        if (log_level == logging.DEBUG) : logger.info(message)
        if (log_level == logging.INFO) : logger.info(message)
        if (log_level == logging.WARNING) : logger.warning(message)     
        if (log_level == logging.ERROR) : logger.error(message)              
        if (log_level == logging.CRITICAL) : logger.critical(message)

    def init_logger(self, logger_name = None) :
        """ Init a new logger from the init object """
        if (logger_name == None): logger_name = str(__name__)
        logger = FalconLoggerInit().get_logger(logger_name, None)
        return logger