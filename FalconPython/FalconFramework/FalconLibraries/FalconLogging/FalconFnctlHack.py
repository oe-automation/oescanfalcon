###############################################################################################################
#                                               IMPORTS AND LIBS                                              #
###############################################################################################################
import filecmp
import os
import win32con
import win32file
import pywintypes

###############################################################################################################
#                                              CLASS DEFINITION                                               #
###############################################################################################################
class LockException(Exception):
    # Error codes:
    LOCK_FAILED = 1

###############################################################################################################
#                                              CLASS DEFINITION                                               #
###############################################################################################################
class FalconFnctlHack: 
    """
        +----------------------------------------------------------------------------------------------------+
        | Description:                                                                                       |
        |      Windows workaround to enable use of the FNCLT Lib which normally only works on NIX systems    |
        |      Includes an unlock and lock method only.                                                      |
        |                                                                                                    |
        +----------------------------------------------------------------------------------------------------+
        | Methods From BaseClassLayout:                                                                      |
        |     --> lock(file, flags)           Runs the windows converted version of FNCTL.lock               |
        |                                     Allocates a file object using a locker object and stores it.   |  
        |                                                                                                    |
        |     --> unlock(file)                Runs the windows converted version of FNCTL.unlock             |
        |                                     This releases and resets the locker object and gives us a file |
        |                                     object we can modify again                                     |
        |                                                                                                    |
        +----------------------------------------------------------------------------------------------------+    
    """

    ###########################################################################################################
    #                                           CLASS VARIABLES                                               #
    ###########################################################################################################

    __overlapped = pywintypes.OVERLAPPED()          # Overlapper type for winapi call to fnctl methods

    # Lock Status objectect
    LOCK_EX = win32con.LOCKFILE_EXCLUSIVE_LOCK      
    LOCK_SH = 0 # the default
    LOCK_NB = win32con.LOCKFILE_FAIL_IMMEDIATELY

    ###########################################################################################################
    #                                           CLASS FUNCTIONS                                               #
    ###########################################################################################################
    
    def lock(file, flags):
        """ FNCTL Lock function workaround """
        # file_stat = os.stat(open(file, "r")).st_ino
        hfile = win32file._get_osfhandle(file.fileno())
        try:
            win32file.LockFileEx(hfile, flags, 0, -0x10000, FalconFnctlHack.__overlapped)
        except pywintypes.error as exc_value:
            if exc_value[0] == 33:
                raise LockException(LockException.LOCK_FAILED, exc_value[2])
            else:
                raise
    
    def unlock(file):
        """ FNCTL Unlock function workaround """
        # file_stat = os.stat(open(file, "r")).st_ino
        hfile = win32file._get_osfhandle(file.fileno())
        try:
            win32file.UnlockFileEx(hfile, 0, -0x10000, FalconFnctlHack.__overlapped)
        except pywintypes.error as exc_value:
            if exc_value[0] == 158:
                pass
            else:
                raise