###############################################################################################################
#                                               IMPORTS AND LIBS                                              #
############################################################################################################### 
import os              # Filesystem browser
import io              # FS IO Access
import logging         # Logging Format Lib

###############################################################################################################
#                                              COLOR DEFINITIONS                                              #
###############################################################################################################

# Colors and log output format
class LoggerDefinitions(logging.Formatter) :
    GREY = "\x1b[0;37m"
    GREEN = "\x1b[1;32m"
    YELLOW = "\x1b[1;33m"
    RED = "\x1b[1;31m"
    PURPLE = "\x1b[1;35m"
    BLUE = "\x1b[1;34m"
    LIGHT_BLUE = "\x1b[1;36m"
    RESET = "\x1b[0m"
    BLINK_RED = "\x1b[5m\x1b[1;31m"
    FORMAT_STRING = f"[%(asctime)s][%(name)s][%(filename)s:%(lineno)d][%(levelname)s]  :::  %(message)s"

    """Logging Formatter to add colors and count warning / errors"""
    def __init__(self, auto_colorized=True, custom_format: str = None):
        super(LoggerDefinitions, self).__init__()
        self.auto_colorized = auto_colorized
        self.custom_format = custom_format
        self.FORMATS = self.define_format()
        if auto_colorized and custom_format:
            print("WARNING: Ignoring auto_colorized argument because you provided a custom_format")

    def define_format(self):
        """ Setup formatting for logger """
        if self.auto_colorized:
            format_suffix = "[%(levelname)s] - %(message)s"
            format_prefix = f"[{LoggerDefinitions.PURPLE}%(asctime)s{LoggerDefinitions.RESET}]" \
                            f"[{LoggerDefinitions.BLUE}%(name)s{LoggerDefinitions.RESET}]" \
                            f"[{LoggerDefinitions.LIGHT_BLUE}%(filename)s:%(lineno)d{LoggerDefinitions.RESET}] ::: " \

            return {
                logging.DEBUG: format_prefix + LoggerDefinitions.GREEN + format_suffix + LoggerDefinitions.RESET,
                logging.INFO: format_prefix + LoggerDefinitions.GREY + format_suffix + LoggerDefinitions.RESET,
                logging.WARNING: format_prefix + LoggerDefinitions.YELLOW + format_suffix + LoggerDefinitions.RESET,
                logging.ERROR: format_prefix + LoggerDefinitions.RED + format_suffix + LoggerDefinitions.RESET,
                logging.CRITICAL: format_prefix + LoggerDefinitions.BLINK_RED + format_suffix + LoggerDefinitions.RESET
            }

        else:
            if self.custom_format:
                _format = self.custom_format
            else:
                _format = self.FORMAT_STRING
            return {
                logging.DEBUG: _format,
                logging.INFO: _format,
                logging.WARNING: _format,
                logging.ERROR: _format,
                logging.CRITICAL: _format
            }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)