###############################################################################################################
#                                               IMPORTS AND LIBS                                              #
###############################################################################################################
import os                           # FS Browser
import io                           # IO Operations
import logging                      # Logging Format Lib
import logging.handlers             # Logging Handlers
import threading                    # Threading lib
import random                       # RNG
import time                         # File Name Creation helper
import gzip                         # File Compression Setup
import filecmp

from FalconFnctlHack import FalconFnctlHack     # FNCTL Hack

###############################################################################################################
#                                              CLASS DEFINITION                                               #
###############################################################################################################
class LoggerNullHandler(io.StringIO):
    """ Logging Null handler if a log file is not real/the stream is locked """
    def emit(self, record):
        pass
    def write(self, *args, **kwargs):
        pass

###############################################################################################################
#                                              CLASS DEFINITION                                               #
###############################################################################################################
_lock = threading.RLock()        # Locker object from threading for system safe file operations
class FalconLogFileLock(logging.handlers.TimedRotatingFileHandler):
    """
        +----------------------------------------------------------------------------------------------------+
        | Description:                                                                                       |
        |     Generates and stores a thread/access safe file locker for writing logs                         |
        |                                                                                                    |
        +----------------------------------------------------------------------------------------------------+
        | Methods From BaseClassLayout:                                                                      |
        |     --> __init__(SEE METHOD)       Sets up a new logger locking class object. Once generated       |
        |                                     this is consumed by a logger instance which uses this for      |               
        |                                     thread/access safe file operations                             |
        |                                                                                                    |                   
        +----------------------------------------------------------------------------------------------------+    
    """
    
    ###########################################################################################################
    #                                           CLASS VARIABLES                                               #
    ###########################################################################################################


    ###########################################################################################################
    #                                           CLASS FUNCTIONS                                               #
    ###########################################################################################################

    def __init__(self, filename, when='midnight', backup_count=30, compress=True, **kwargs):
        super(FalconLogFileLock, self).__init__(filename, when=when, backupCount=backup_count, **kwargs)
        self.compress = compress

    def _open(self):
        if getattr(self, '_lockf', None) and not self._lockf.closed:
            return open(self.baseFilename, self.mode, encoding=self.encoding)
        with _lock:
            while True:
                try:
                    self._aquire_lock()
                    return logging.handlers.TimedRotatingFileHandler._open(self)
                except (IOError, BlockingIOError):
                    self._release_lock()
                    time.sleep(random.random())
                finally:
                    self._release_lock()
    
    def _aquire_lock(self):
        try:
            self._lockf = open(self.baseFilename + '_rotating_lock', 'a')
        except PermissionError:
            name = './{}_rotating_lock'.format(os.path.basename(self.baseFilename))
            self._lockf = open(name, 'a')
        FalconFnctlHack.lock(self._lockf, FalconFnctlHack.LOCK_EX | FalconFnctlHack.LOCK_NB)

    def _release_lock(self):
        if not self._lockf.closed:
            FalconFnctlHack.unlock(self._lockf)
            self._lockf.close()

    @staticmethod
    def is_same_file(file1, file2):
        """ Check is files are same by comparing inodes """
        stat1 = os.stat(open(file1, "r"))
        stat2 = os.stat(open(file2, "r'"))
        return stat1.st_ino == stat2.st_ino and stat1.st_dev == stat2.st_dev

    def doRollover(self):
        """
            Do a rollover; in this case, a date/time stamp is appended to the filename
            when the rollover happens.  However, you want the file to be named for the
            start of the interval, not the current time.  If there is a backup count,
            then we have to get a list of matching filenames, sort them and remove
            the one with the oldest suffix.
        """
        with _lock:
            return self._innerDoRollover()

    def _innerDoRollover(self):
        try:
            self._acquire_lock()
        except (IOError, BlockingIOError):
            # cant acquire lock, return
            self._release_lock()
            return

        # get the time that this sequence started at and make it a time_tuple
        t = self.rolloverAt - self.interval
        if self.utc:
            time_tuple = time.gmtime(t)
        else:
            time_tuple = time.localtime(t)

        # check if file is same
        try:
            if self.stream:
                with open(self.baseFilename, 'r') as _tmp_f:
                    is_same = self.is_same_file(self.stream, _tmp_f)

                if self.stream:
                    self.stream.close()

                if is_same:
                    if self.compress:
                        dfn = self.baseFilename + "." + time.strftime(self.suffix, time_tuple) + ".txt" + ".gz"
                        if not os.path.exists(dfn):
                            with open(self.baseFilename, "rb+") as sf:
                                data = sf.read()
                                compressed = gzip.compress(data, 9)
                                with open(dfn, "wb") as df:
                                    df.write(compressed)
                            os.remove(self.baseFilename)
                    else:
                        dfn = self.baseFilename + "." + time.strftime(self.suffix, time_tuple) + ".txt"
                        os.rename(self.baseFilename, dfn)

        except ValueError:
            # ValueError: I/O operation on closed file
            is_same = False

        if self.backupCount > 0:
            for s in self.getFilesToDelete():
                os.remove(s)
        self.mode = 'a'
        self.stream = self._open()
        current_time = int(time.time())
        new_rollover_at = self.computeRollover(current_time)
        while new_rollover_at <= current_time:
            new_rollover_at = new_rollover_at + self.interval

        # If DST changes and midnight or weekly rollover, adjust for this.
        if (self.when == 'MIDNIGHT' or self.when.startswith('W')) and not self.utc:
            dst_now = time.localtime(current_time)[-1]
            dst_at_rollover = time.localtime(new_rollover_at)[-1]
            if dst_now != dst_at_rollover:
                if not dst_now:  # DST kicks in before next rollover, so we need to deduct an hour
                    new_rollover_at = new_rollover_at - 3600
                else:           # DST bows out before next rollover, so we need to add an hour
                    new_rollover_at = new_rollover_at + 3600
        self.rolloverAt = new_rollover_at
        self._release_lock()