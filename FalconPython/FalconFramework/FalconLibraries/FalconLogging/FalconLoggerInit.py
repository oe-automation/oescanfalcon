###############################################################################################################
#                                               IMPORTS AND LIBS                                              #
###############################################################################################################
import os                           # FS Browser
import io                           # IO Operation
import ntpath
import logging                      # Logging Format Lib
import FalconMainVariables          # Main Variable Share

from FalconLogFileLock import LoggerNullHandler          # Null stream helper
from FalconLogFileLock import FalconLogFileLock           # ThreadSafe importing
from LoggerDefinitions import LoggerDefinitions           # Logger Coloring

###############################################################################################################
#                                              CLASS DEFINITION                                               #
###############################################################################################################
class FalconLoggerInit: 
    """ 
        +----------------------------------------------------------------------------------------------------+
        | Description:                                                                                       |
        |     Generates a new logging object and configures it for the specified output file                 |
        |     The output file can either be named or left blank. If it's blank, we use the value setup in    |
        |     the global configuration                                                                       |
        +----------------------------------------------------------------------------------------------------+
        | Methods From LaunchOEApp:                                                                          |
        |     --> log_format()                    Generates a log format string based on value               |
        |                                                                                                    |
        |     --> get_logger()                    Gets a new or existing logger based on the set name value  |
        |                                         passed into it as a paramater.                             |
        |                                                                                                    |
        |     --> __add_file_logger__()           Adds a file logger to the logging root object              |
        |     --> __add_console_logger()          Adds a console logger to the logging root object           |
        +----------------------------------------------------------------------------------------------------+     
    """

    ###########################################################################################################
    #                                           CLASS FUNCTIONS                                               #
    ###########################################################################################################

    RAN_OUTPUTS_ONCE = False
    def get_logger(self, name = None, filename = None):
        """ Gets the logger object for this application """

        # Build file path and setup output dir if needed
        if (filename == None): filename = str(FalconMainVariables.FALCON_LOG_FILE)
        full_file = str(FalconMainVariables.FALCON_LOGGING_PATH) + filename
        if not os.path.exists(os.path.dirname(full_file)):
            os.makedirs(os.path.dirname(full_file))

        logging.basicConfig(level=logging.DEBUG, stream=LoggerNullHandler())
        if (FalconLoggerInit.RAN_OUTPUTS_ONCE == False):
            if (FalconLoggerInit.RAN_OUTPUTS_ONCE == False):
                # Write splitter
                file_obj = open(filename, "a")
                file_obj.write("#######################################################################################################################################\n")
                file_obj.close()

            # Set Logging configuration
            FalconLoggerInit.__add_console_logger__()
            FalconLoggerInit.__add_file_logger__(filename, "midnight", 1, 10, True)
        
            # Force stop output render
            FalconLoggerInit.RAN_OUTPUTS_ONCE = True
            
        # Return the logger object here.
        if (name == None): name = __name__
        logger_object = logging.getLogger(str(name))
        return logger_object

    def log_format(self, log_string) : 
        """ Generates a formatted log output string value """
        log_fmt = self.LOG_FORMATS.get(log_string.levelno)
        log_formatter = logging.Formatter(log_fmt)
        return log_formatter.format(log_string)
        
        
    def __add_file_logger__(filename, when, interval, backup_count, compress) : 
        """ Adds a file logger to the logging root """
        # Build file locker/Logger
        file_logger = FalconLogFileLock(
            filename=filename, when=when, interval=interval,
            backup_count=backup_count, compress=compress
        )

        # Configure logger settings.
        file_logger.setLevel(logging.DEBUG)
        file_logger.setFormatter(LoggerDefinitions(False, None))
        logging.root.addHandler(file_logger)

    def __add_console_logger__() : 
        """ Adds a console logger to the logging root """
        # Setup console logger
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.DEBUG)
        console_handler.setFormatter(LoggerDefinitions(True, None))
        logging.root.addHandler(console_handler)


        