###################################################################################################
#                                       SETUP FOR ROBOT                                           #
###################################################################################################

*** Settings ***
Documentation     Sets up any of the main types of robotframework automation scripts.
...
...               This robot configures the main launcher of a Falconized OEScan. 
...               First we load up a type of scan object from a JSON File and then parse it's 
...               contents. Once loaded, we begin setting up a main robot.

# Resource Files/Custom Libraries
Library           FalconLogger                # Library for logging
Variables         FalconMainVariables.py          # Main Variable object
Resource          FalconStartupKeywords.robot     # Configuration for logging and JSON

###################################################################################################
#                                     ROBOT METHOD DEFINITIONS                                    #
###################################################################################################

*** Tasks ***
Falcon Session Startup
    [Tags]                  SCAN_SETUP_TASK
    [Documentation]         This Test Case loads the python path environment then spins up a JSON 
    ...                     configuration file for the main falcon OE Scan process and then 
    ...                     configures a scan session type json config object based on the input 
    ...                     argument type
 
    # Start logging environment
    ${LOGGER_NAME}=    Set Variable    FALCON_LAUNCHER
    Init Logger    logger_name=${LOGGER_NAME}
    Setup Falcon Logging
    Log Message    logger_name=${LOGGER_NAME}    message=Launching Configuration Tasks for Falcon Now... 

    # Setup Environment
    Log Message   logger_name=${LOGGER_NAME}     message=Setting Up Scan Environment...
    Setup Falcon Environment   TIS
    
    # Launch the OE App
    Log Message   logger_name=${LOGGER_NAME}     message=Launching OE Scan Application...
    Start OE Application
    