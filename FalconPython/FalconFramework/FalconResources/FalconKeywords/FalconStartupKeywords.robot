###################################################################################################
#                                       SETUP FOR ROBOT                                           #
###################################################################################################

*** Settings ***
Library        FalconLogger                  # Library for logging
Library        FalconLoggerInit              # Library for logging startup
Library        FalconJsonConfig              # JSON Importing for Falcon
Variables      FalconMainVariables.py        # Main Variable object

###################################################################################################
#                                    ROBOT KEYWORD DEFINITIONS                                    #
###################################################################################################

*** Keywords *** 
Setup Falcon Logging
    [Documentation]         Boots up a new set of values for the log file values

    # Store as suite value
    ${GLOBAL_LOG_FILE}=     Set Variable          ${FALCON_LOG_FILE}
    Set Suite Variable 	    ${FALCON_LOG_FILE}    ${FALCON_LOG_FILE}

    # Store name and write info
    ${LOGGER_NAME}=    Set Variable             SetupFalconLogging
    Init Logger    logger_name=${LOGGER_NAME}
    Log Message    logger_name=${LOGGER_NAME}   message=SET GLOBAL LOG FILE TO ${GLOBAL_LOG_FILE}    

Setup Falcon Environment
    [Arguments]             ${scan_type}=TIS
    [Documentation]         Configures a new Falcon Scan Logger object and stores it for later use
    ...                     at any point during this apps lifetime.
    ...                     Once done, this Case loads up a JSON config file for the main falcon 
    ...                     OE Scan process and then configures a scan session type json config 
    ...                     object based on the input argument types

    # Get the Base Config Setup and then the Scan Setup
    ${LOGGER_NAME}=    Set Variable            SetupFalconEnvironment
    Init Logger   logger_name=${LOGGER_NAME}
    Log Message   logger_name=${LOGGER_NAME}   message=Setting Up App JSON Configuration
    Import Base Config
    Log Message   logger_name=${LOGGER_NAME}   message=IMPORTED LOCAL JSON FOR MAIN CONFIG  
    Import Scan Config      ${scan_type}    
    Log Message  logger_name=${LOGGER_NAME}    message=IMPORTED LOCAL JSON FOR SCAN SESSION CONFIG

Start OE Application
    [Documentation]         Launches the OE App in our app configuration setup 
    ...                     Once opened, this keyword pulls the controls of the currently opened OE app
    ...                     at its main entry point into a controls storage object.

    # Launch the application here.
    ${LOGGER_NAME}=    Set Variable              StartOEApplication
    Init Logger    logger_name=${LOGGER_NAME}
    Log Message    logger_name=${LOGGER_NAME}    message=LAUCNHED OE APP OK!   