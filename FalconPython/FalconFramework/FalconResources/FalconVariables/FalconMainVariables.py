""" 
    This file is used for environment configuration/variables.                                     
    So these values are accessable from all robots, keywords, classes, libs etc.                     
                                                                                     
    +=================================================================================================+
    | Variable Name       | Default Value | Variable Use                                              |
    | =================== | ============= | ========================================================= |
    | LIBRARY_BASE_PATH   | String        | Contains the path location of all supporting Falcon       |
    |                     |               | python libraries used for most actions in this robot      | 
    +---------------------+---------------+-----------------------------------------------------------+
    | RESOURCES_BASE_PATH | String        | Contains the path location of all supporting Falcon       |
    |                     |               | python resource files used for most actions in this robot | 
    +---------------------+---------------+-----------------------------------------------------------+ 
    | SCAN_CONFIGS_PATH   | String        | Contains the path location of all JSON Falcon Scan config |
    |                     |               | files. These are loaded to determine output actions       | 
    +---------------------+---------------+-----------------------------------------------------------+
    | FALCON_LOGGING_PATH | String        | Base falcon logging output location. Combined with a file |
    |                     |               | name to generate an output value                          |
    +=================================================================================================+
"""

#####################################################################################################
#                                          Variable Definitions                                     #
#####################################################################################################

# Resources and Path
RESOURCES_BASE_PATH = "../../FalconResources/"
LIBRARIES_BASE_PATH = "../../FalconLibraries/"
SCAN_CONFIGS_PATH = RESOURCES_BASE_PATH + "FalconJson/"

# Log File Configuration
FALCON_LOGGING_PATH = "../../../FalconOutput/"
FALCON_LOG_FILE = "SetupFalconEnvironemnt.log"
