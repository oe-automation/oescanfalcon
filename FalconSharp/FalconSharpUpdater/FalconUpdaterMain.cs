﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3.IO;
using ERSConstructor;
using ERSConstructor.DynamicERS;
using Newtonsoft.Json;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using FalconObjects.FalconParameterObjects;
using Newtonsoft.Json.Linq;

namespace FalconSharpUpdater
{
    /// <summary>
    /// Updates the main app contents.
    /// </summary>
    public class FalconUpdaterMain
    {
        // Logger Objects
        private DynamicERSLogger Logger;

        // Key Locations
        public FalconAWSLocations FileLocations;
        public FalconAWSConfiguration AwsConfiguration;

        // AWS Infos
        private AmazonS3Client S3Client;

        // -----------------------------------------------------------------------------

        /// <summary>
        /// Builds a new AWS invoker.
        /// </summary>
        /// <param name="Bucket">Bucket to pull</param>
        /// <param name="Key">AWS Key</param>
        /// <param name="Secret">AWS Password</param>
        public FalconUpdaterMain(DynamicERSLogger UpdatingLogger, string Bucket, string Key, string Secret, bool SuperLogging = false)
        {
            // Store Logger 
            this.Logger = UpdatingLogger;

            // Store AWS config values first.
            this.AwsConfiguration = new FalconAWSConfiguration();
            this.AwsConfiguration.S3BucketName = Bucket;
            this.AwsConfiguration.S3AccessKey = Key;
            this.AwsConfiguration.S3SecretKey = Secret;

            // Write information
            Logger.WriteLogEntry("UPDATER READ IN STRINGS FOR AWS VALUES", LogEntryType.LOG_WARN);
            Logger.WriteLogEntry("NO NEED FOR CONVERSIONS. THIS MAY BE INCONSISTENT WITH MAIN!", LogEntryType.LOG_WARN);
            this.FalconUpdaterStarted();
        }
        /// <summary>
        /// Builds a new falcon configuration object using an AWS configuration 
        /// </summary>
        /// <param name="UpdatingLogger">Logger</param>
        /// <param name="AWSConfiguration">AWS Object to parse out. Must follow layout of one of the many AWS objects</param>
        public FalconUpdaterMain(DynamicERSLogger UpdatingLogger, dynamic AWSConfiguration, bool SuperLogging = false)
        {
            // Store Logger 
            this.Logger = UpdatingLogger;
            
            // Build and store config
            string JsonConverted = JsonConvert.SerializeObject(AWSConfiguration);
            if (SuperLogging) { JsonConverted = "\n" + JsonConverted; }
            var Config = JsonConvert.DeserializeObject<FalconAWSConfiguration>(JsonConverted);
            this.AwsConfiguration = Config;

            // Write information
            Logger.WriteLogEntry("UPDATER LOGGER CONVERTED DYNAMIC OBJECT INTO A FALCON AWS CONFIG OK!", LogEntryType.LOG_INFO);
            Logger.WriteLogEntry($"OBJECT INPUT AS A JSON VALUE WAS: {JsonConverted}", LogEntryType.LOG_TRACE);
            this.FalconUpdaterStarted();
        }

        /// <summary>
        /// Writes updater launched ok output values
        /// </summary>
        private void FalconUpdaterStarted()
        {
            // Generate File Locations
            string JsonInput = File.ReadAllText("FalconResources\\FalconSettings.json");
            JObject ConfigFileContent = JObject.Parse(JsonInput);
            this.FileLocations = ConfigFileContent["FileLocations"].ToObject<FalconAWSLocations>();

            // Make an S3 Client
            this.S3Client = new AmazonS3Client(
                this.AwsConfiguration.S3AccessKey, this.AwsConfiguration.S3SecretKey,
                RegionEndpoint.EUWest1
            );

            // Write new values out here.
            Logger.WriteLogEntry("--- FALCON UPDATER CTOR STATUS ---", LogEntryType.LOG_INFO);
            Logger.WriteLogEntry("READ AND IMPORTED AWS KEY VALUES OK!", LogEntryType.LOG_DEBUG);
            Logger.WriteLogEntry("GOT NEW AWS VALUES!", LogEntryType.LOG_DEBUG);
            Logger.WriteLogEntry($"S3Bucket: {this.AwsConfiguration.S3BucketName}", LogEntryType.LOG_TRACE);
            Logger.WriteLogEntry($"S3Access: {this.AwsConfiguration.S3AccessKey}", LogEntryType.LOG_TRACE);
            Logger.WriteLogEntry($"S3Secret: {this.AwsConfiguration.S3SecretKey}", LogEntryType.LOG_TRACE);
            Logger.WriteLogEntry("SETUP NEW FALCON UPDATER OK!", LogEntryType.LOG_INFO);
        }

        #region PYTHON ENGINE UPDATING
        /// <summary>
        /// Updates only the ironpython install output
        /// </summary>
        /// <param name="IronPythonOutput">Path to dump the ironpy libs</param>
        public void UpdateIronPyStd(string IronPythonOutput = "")
        {
            // Set IronPy Path
            if (string.IsNullOrEmpty(IronPythonOutput)) IronPythonOutput = this.FileLocations.IronPythonLocalBase + "\\IronPythonStd";
            Logger.WriteLogEntry("UPDATING IRON PYTHON STANDARD LIBRARIES...", LogEntryType.LOG_WARN);
            Logger.WriteLogEntry($"IRONPYTHON STD LIB: {FileLocations.IronPythonStdLib}", LogEntryType.LOG_DEBUG);
            Logger.WriteLogEntry($"WRITING NEW VALUES TO: {IronPythonOutput}", LogEntryType.LOG_DEBUG);

            // Get the Dir info and then list all files in this folder.
            string FormattedKey = FileLocations.IronPythonStdLib.Replace("/", "\\");
            var DirInfos = new S3DirectoryInfo(S3Client, this.AwsConfiguration.S3BucketName, FormattedKey);
            var StdLibInfos = DirInfos.GetFileSystemInfos();

            // Download the files now.
            Logger.WriteLogEntry($"GOT BACK A TOTAL OF {StdLibInfos.Length} FILES AND DIRS FROM THE BUCKET", LogEntryType.LOG_INFO);
            Logger.WriteLogEntry("DOWNLOADING THEM NOW...", LogEntryType.LOG_INFO);

            // DOWNLOAD THE FILES
            this.DownloadIS3Objects(FormattedKey, IronPythonOutput, StdLibInfos);
            Logger.WriteLogEntry("DOWNLOADED ALL NEW FILES AND DIRS CORRECTLY!", LogEntryType.LOG_INFO);
        }
        /// <summary>
        /// Updates the Ironpython scripts on machine from the AWS bucket.
        /// </summary>
        /// <param name="IronPythonOutput">Output path location</param>
        public void UpdateIronPyScripts(string IronPythonOutput = "")
        {
            // Set IronPy Path
            if (string.IsNullOrEmpty(IronPythonOutput)) IronPythonOutput = this.FileLocations.IronPythonLocalBase + "\\IronPythonScripts";
            Logger.WriteLogEntry("UPDATING IRON PYTHON SCRIPTING RESOURCES...", LogEntryType.LOG_WARN);
            Logger.WriteLogEntry($"IRONPYTHON SCRIPTS: {FileLocations.IronPythonScripts}", LogEntryType.LOG_DEBUG);
            Logger.WriteLogEntry($"WRITING NEW VALUES TO: {IronPythonOutput}", LogEntryType.LOG_DEBUG);

            // Get the Dir info and then list all files in this folder.
            string FormattedKey = FileLocations.IronPythonScripts.Replace("/", "\\");
            var DirInfos = new S3DirectoryInfo(S3Client, this.AwsConfiguration.S3BucketName, FormattedKey);
            var ScriptInfos = DirInfos.GetFileSystemInfos();

            // Download the files now.
            Logger.WriteLogEntry($"GOT BACK A TOTAL OF {ScriptInfos.Length} FILES AND DIRS FROM THE BUCKET", LogEntryType.LOG_INFO);
            Logger.WriteLogEntry("DOWNLOADING THEM NOW...", LogEntryType.LOG_INFO);

            // DOWNLOAD THE FILES
            this.DownloadIS3Objects(FormattedKey, IronPythonOutput, ScriptInfos);
            Logger.WriteLogEntry("DOWNLOADED ALL NEW FILES AND DIRS CORRECTLY!", LogEntryType.LOG_INFO);
        }
        #endregion

        /// <summary>
        /// Recurse Download objects from a bucket given the file infos.
        /// </summary>
        /// <param name="BaseKey">formatted path key</param>
        /// <param name="BaseOutput">Download Dir</param>
        /// <param name="InputObjects">Objects to dl</param>
        private void DownloadIS3Objects(string BaseKey, string BaseOutput, IS3FileSystemInfo[] InputObjects)
        {
            // Build downloading object and setup a stopwatch.
            var ObjDownloader = new TransferUtility(S3Client);
            Stopwatch TimerWatch = new Stopwatch();
            TimerWatch.Start();

            // Loop the contents of our IS3Infos and download them in parallel.
            Parallel.ForEach(InputObjects, ObjInfos =>
            {
                try
                {
                    // For File Objects
                    if (ObjInfos.Extension != "")
                    {
                        string FileOutput = Path.Combine(BaseOutput, ObjInfos.Name);
                        string ObjectKey = (BaseKey + ObjInfos.Name).Replace("\\", "/");
                        if (!File.Exists(FileOutput))
                        {
                            ObjDownloader.Download(FileOutput, this.AwsConfiguration.S3BucketName, ObjectKey);
                            Logger.WriteLogEntry($"--> DOWNLOADED FILE: {ObjInfos.Name} (TIME ELAPSED: {TimerWatch.Elapsed.ToString("g")})", LogEntryType.LOG_TRACE);
                        }
                    }
                    else
                    {
                        // For DIR Objects
                        string FileOutput = Path.Combine(BaseOutput, ObjInfos.Name);
                        string ObjectKey = BaseKey + ObjInfos.Name;
                        Directory.CreateDirectory(FileOutput);
                        GetSubDirObjects(ObjectKey, FileOutput);
                    }
                }
                catch { Logger.WriteLogEntry($"ERROR ON FILE: {ObjInfos.Name}", LogEntryType.LOG_ERROR); }
            });

            // Log Done
            Logger.WriteLogEntry($"TIME ELAPSED: {TimerWatch.Elapsed.ToString("g")}!", LogEntryType.LOG_DEBUG);
        }
        /// <summary>
        /// Recusive call to get object subcontents.
        /// </summary>
        /// <param name="NextKey"></param>
        /// <param name="BaseOutput"></param>
        private void GetSubDirObjects(string NextKey, string BaseOutput)
        {
            // Make downloader
            var ObjDownloader = new TransferUtility(S3Client);

            // Get next info set.
            var SubDirInfos = new S3DirectoryInfo(S3Client, this.AwsConfiguration.S3BucketName, NextKey);
            var SubInfos = SubDirInfos.GetFileSystemInfos();
            Parallel.ForEach(SubInfos, ObjInfos =>
            {
                string OutputLocation = Path.Combine(BaseOutput, ObjInfos.Name);
                string ObjectKey = (NextKey + "\\" + ObjInfos.Name).Replace("\\", "/");
                if (ObjInfos.Extension == "")
                {
                    // Build DIR
                    string DirName = new FileInfo(OutputLocation).DirectoryName;
                    Directory.CreateDirectory(DirName);

                    // Get Dir infos.
                    Logger.WriteLogEntry($"--> RECURSIVELY SEARCHING DIR: {DirName.Split('\\').Last()}", LogEntryType.LOG_DEBUG);
                    GetSubDirObjects(ObjectKey, DirName);
                }
                else
                {
                    // Don't Download Dupe files
                    if (File.Exists(OutputLocation)) return;

                    // Make output if needed here for just a file.
                    ObjDownloader.Download(OutputLocation, this.AwsConfiguration.S3BucketName, ObjectKey);
                    Logger.WriteLogEntry($"--> DOWNLOADED SUBDIR FILE: {ObjInfos.Name}", LogEntryType.LOG_TRACE);
                }
            });
        }
    }
}
