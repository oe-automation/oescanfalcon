﻿using ERSConstructor.DynamicERS;
using FalconTalons.Invokers;
using FalconTalons.Models;
using FalconTalons.Models.Talons;
using System.Drawing;
using System.Windows.Forms;

namespace FalconTalons.ViewGeneration
{
    /// <summary>
    /// Paints an outline around the provided control
    /// </summary>
    public class TalonPainter
    {
        // The Element to outline.
        public TalonElement Element;

        // Color for outline and background
        private Font NameFont;
        private Color TextColor;
        private Color OutlineColor;
        private Color BackgroundColor;

        // Layout Configuration
        private Graphics FormGfx;
        private Pen ControlOutlinePen;
        private RectangleF BoundingBoxRectangle;

        // -----------------------------------------------------------------------------

        /// <summary>
        /// Builds a new painter
        /// </summary>
        /// <param name="ElementToPaint">Object to paint</param>
        public TalonPainter(TalonElement ElementToPaint)
        {
            // Store element and get paint colors.
            this.Element = ElementToPaint;

            // Build and Show outline
            this.GenerateGraphics();
            TalonSession.TalonLogger.WriteLogEntry($"ELEMENT PAINTER FOR {Element.Name} HAS BEEN LAUNCHED OK", LogEntryType.LOG_TRACE);
        }


        /// <summary>
        /// Builds the GFX engine to display with.
        /// </summary>
        private void AssembleGraphicsEngine()
        {
            // Generate Dynamic shape for this
            var ElementRectange = this.Element.BoundingRectangle;
            this.FormGfx = Graphics.FromHwnd(this.Element.MainWindowPointer);

            // Set Bounding Box.
            this.BoundingBoxRectangle = new RectangleF(
                (float)ElementRectange.Left, (float)ElementRectange.Top,
                (float)ElementRectange.Width, (float)ElementRectange.Height
            );
        }
        /// <summary>
        /// Set the colors, sizes, fonts and location of this control outline
        /// </summary>
        public void GenerateGraphics(Color ColorBase = default)
        {
            // Build GFX Engine
            this.AssembleGraphicsEngine();

            // Get color and check for custom type.
            this.BackgroundColor = Color.Black;
            if (ColorBase != default) { this.OutlineColor = ColorBase; }
            else
            {
                var CtlName = this.Element.ElementBase.Current.ControlType.ProgrammaticName;
                if (CtlName.Contains("Button")) this.BackgroundColor = Color.Red;
                if (CtlName.Contains("Bar")) this.BackgroundColor = Color.Blue;
                if (CtlName.Contains("Pane")) this.BackgroundColor = Color.Yellow;
                if (CtlName.Contains("Window")) this.BackgroundColor = Color.Orange;
            }

            // Set Supporting Colors.
            this.TextColor = this.OutlineColor.IsDark() ? Color.Black : Color.White;
            this.NameFont = new Font(FontFamily.GenericMonospace, 12.0f, FontStyle.Bold);
            this.OutlineColor = this.BackgroundColor.ShadeColor(this.BackgroundColor.IsDark() ? Color.Black : Color.White, .50f);
            this.ControlOutlinePen = new Pen(this.OutlineColor);
            this.BackgroundColor = this.BackgroundColor.ShadeColor(Color.Transparent, .60f);
        }


        /// <summary>
        /// Paint content onto the window.
        /// </summary>
        public void ShowControlOutline(Color CustomColor = default)
        {
            // Draw Text Values And Bounding Box Now.
            this.GenerateGraphics(CustomColor);
            this.FormGfx.FillRectangle(this.BackgroundColor.ToBrush(), Rectangle.Round(this.BoundingBoxRectangle));
            this.FormGfx.DrawRectangle(this.ControlOutlinePen, Rectangle.Round(this.BoundingBoxRectangle));
            this.FormGfx.DrawString(this.Element.Name, this.NameFont, this.TextColor.ToBrush(), this.BoundingBoxRectangle);
        }
        /// <summary>
        /// Hides the control outline and label
        /// </summary>
        public void HideControlOutline()
        {

        }
    }
}
