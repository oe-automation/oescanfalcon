﻿using System;
using System.Globalization;

namespace FalconTalons.ViewGeneration
{
    public static class CustomColorConverter
    {
        /// <summary>
        /// Convert Media Color (WPF) to Drawing Color (WinForm)
        /// </summary>
        /// <param name="mediaColor"></param>
        /// <returns></returns>
        public static System.Drawing.Color ToDrawingColor(this System.Windows.Media.Color mediaColor)
        {
            return System.Drawing.Color.FromArgb(mediaColor.A, mediaColor.R, mediaColor.G, mediaColor.B);
        }
        /// <summary>
        /// Convert Drawing Color (WPF) to Media Color (WinForm)
        /// </summary>
        /// <param name="drawingColor"></param>
        /// <returns></returns>
        public static System.Windows.Media.Color ToMediaColor(this System.Drawing.Color drawingColor)
        {
            return System.Windows.Media.Color.FromArgb(drawingColor.A, drawingColor.R, drawingColor.G, drawingColor.B);
        }
        /// <summary>
        /// Generates a color from an input string.
        /// </summary>
        /// <param name="colorString">The string to convert out</param>
        /// <returns>A color from the string given</returns>
        public static System.Drawing.Color FromStringToDrawing(this string colorString)
        {
            // Convert the color
            int ColorUint = int.Parse(colorString.Replace("0x", String.Empty), NumberStyles.HexNumber);
            var OutputColor = System.Drawing.Color.FromArgb(ColorUint);

            // Return the color
            return OutputColor;
        }
        /// <summary>
        /// Generates a color from an input string.
        /// </summary>
        /// <param name="colorString">The string to convert out</param>
        /// <returns>A color from the string given</returns>
        public static System.Windows.Media.Color FromStringToMedia(this string colorString)
        {
            // Convert the color
            var ColorObj = colorString.FromStringToDrawing();
            var OutputColor = ColorObj.ToMediaColor();

            // Return the color
            return OutputColor;
        }

        /// <summary>
        /// Color shader.
        /// </summary>
        /// <param name="InputColor">Color to shade from</param>
        /// <param name="Comparison">Color to shade into</param>
        /// <param name="ChangeBy">FLoat value to change the color by.</param>
        /// <returns>Color object of the shaded result.</returns>
        public static System.Windows.Media.SolidColorBrush ToSolidBrush(this System.Windows.Media.Color InputColor, System.Windows.Media.Color Comparison = default, float ChangeBy = 0)
        {
            if (Comparison == default) { ChangeBy = 0f; }
            return new System.Windows.Media.SolidColorBrush(ColorShader.ShadeColor(InputColor, Comparison, ChangeBy));
        }
        /// <summary>
        /// Color shader.
        /// </summary>
        /// <param name="InputColor">Color to shade from</param>
        /// <param name="Comparison">Color to shade into</param>
        /// <param name="ChangeBy">FLoat value to change the color by.</param>
        /// <returns>Color object of the shaded result.</returns>
        public static System.Windows.Media.SolidColorBrush ToSolidBrush(this System.Drawing.Color InputColor, System.Drawing.Color Comparison = default, float ChangeBy = 0)
        {
            if (Comparison == default) { ChangeBy = 0f; }
            return new System.Windows.Media.SolidColorBrush(
                ColorShader.ShadeColor(InputColor.ToMediaColor(), Comparison.ToMediaColor(), ChangeBy)
            );
        }
        
        /// <summary>
        /// Converts a color to a brush
        /// </summary>
        /// <param name="InputColor"></param>
        /// <param name="Comparison"></param>
        /// <param name="ChangeBy"></param>
        /// <returns></returns>
        public static System.Drawing.Brush ToBrush(this System.Windows.Media.Color InputColor, System.Windows.Media.Color Comparison = default, float ChangeBy = 0)
        {
            if (Comparison == default) { ChangeBy = 0f; }
            InputColor = ColorShader.ShadeColor(InputColor, Comparison, ChangeBy);
            System.Drawing.Brush CastBrush = new System.Drawing.SolidBrush(InputColor.ToDrawingColor());
            return CastBrush;
        }
        /// <summary>
        /// Converts a color to a brush
        /// </summary>
        /// <param name="InputColor"></param>
        /// <param name="Comparison"></param>
        /// <param name="ChangeBy"></param>
        /// <returns></returns>
        public static System.Drawing.Brush ToBrush(this System.Drawing.Color InputColor, System.Drawing.Color Comparison = default, float ChangeBy = 0)
        {
            if (Comparison == default) { ChangeBy = 0f; }
            InputColor = ColorShader.ShadeColor(InputColor.ToMediaColor(), Comparison.ToMediaColor(), ChangeBy).ToDrawingColor();
            System.Drawing.Brush CastBrush = new System.Drawing.SolidBrush(InputColor);
            return CastBrush;
        }

        // ---------------------------------------------------------------------------------------------


        /// <summary>
        /// Convert Drawing Color a String
        /// </summary>
        /// <param name="InputColor">Color to convert</param>
        /// <returns>string converted color value</returns>
        public static string HexConverter(System.Drawing.Color InputColor)
        {
            return (InputColor.R.ToString("X2") + InputColor.G.ToString("X2") + InputColor.B.ToString("X2")).ToUpper();
        }
        /// <summary>
        /// Convert Media Color a String
        /// </summary>
        /// <param name="InputColor">Color to convert</param>
        /// <returns>string converted color value</returns>
        public static string RGBConverter(System.Drawing.Color InputColor)
        {
            return (InputColor.R.ToString() + "," + InputColor.G.ToString() + "," + InputColor.B.ToString()).ToUpper();
        }


        /// <summary>
        /// Sets if a color is light or dark
        /// </summary>
        /// <returns></returns>
        public static bool IsDark(this System.Drawing.Color InputColor)
        {
            return (InputColor.R * 0.2126 + InputColor.G * 0.7152 + InputColor.B * 0.0722 > 255 / 2);
        }
        /// <summary>
        /// Sets if a color is light or dark
        /// </summary>
        /// <returns></returns>
        public static bool IsDark(this System.Windows.Media.Color InputColor)
        {
            var ColorDrawing = InputColor.ToDrawingColor();
            return (ColorDrawing.R * 0.2126 + ColorDrawing.G * 0.7152 + ColorDrawing.B * 0.0722 > 255 / 2);
        }
    }
}
