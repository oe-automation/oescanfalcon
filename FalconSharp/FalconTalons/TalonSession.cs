﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Forms;
using ERSConstructor;
using ERSConstructor.DynamicERS;
using FalconTalons.Invokers;
using FalconTalons.Models.Talons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FalconTalons
{
    /// <summary>
    /// Static constants for talons.
    /// </summary>
    public class TalonSession
    {
        // Talon Static objects
        private Window FalconWindow;
        private string OutputFolderName;
        internal static bool SuperLogging = false;
        internal static DynamicERSLogger TalonLogger;

        // TODO: MAKE A THREAD WHICH UPDATES THE CONTENTS OF THE WINDOW ELEMENTS ITEM EVERY N SECONDS

        // Process Object and Contents
        public TalonWindow WindowTalon;
        public TalonElement[] WindowElements => WindowTalon?.WindowElements;
        public TalonExecutable WindowProcess => WindowTalon?.WindowExecutable;
        public TalonElement WindowTalonElement => WindowTalon?.RootWindowTalon;

        // Pointers and Status for App Session
        public Process AppProcess => WindowTalon?.AppProcess;
        public string ApplicationName => WindowTalon?.ApplicationName;
        public bool AppIsRunning => AppProcess == null ? false : WindowTalon.AppIsRunning;
        public IntPtr ProcessPointer => AppProcess == null ? IntPtr.Zero : AppProcess.Handle;
        public IntPtr MainWindowPointer => AppProcess == null ? IntPtr.Zero : AppProcess.MainWindowHandle;

        // Locks windows in place.
        [JsonIgnore]
        public WindowRelocator RootWindowLocker;            // Main OE App
        [JsonIgnore]
        public WindowRelocator ConsoleWindowLocker;         // Console Output
        [JsonIgnore]
        public WindowRelocator TalonDissectionLocker;       // Talon Inspector

        // -----------------------------------------------------------------------------

        /// <summary>
        /// Builds a new talon broker using a logger instance or from json configs.
        /// </summary>
        public TalonSession(Window MainFalconWindow, string TalonJsonLocation, bool DeepLogging = false)
        {
            // Set Output Location
            SuperLogging = DeepLogging;
            this.OutputFolderName = Path.Combine(TalonJsonLocation, "FalconTalons");
            Directory.CreateDirectory(this.OutputFolderName);

            // Build Logging
            TalonLogger = LoggingHelpers.MainLogger;

            // Set Main Window Pointer
            this.FalconWindow = MainFalconWindow;

            // Write Information
            TalonLogger.WriteLogEntry("PROCESSED SETTINGS JSON FILE CORRECTLY AND SETUP LOGGING CONFIG FOR TALON", LogEntryType.LOG_DEBUG);
            TalonLogger.WriteLogEntry("READ AND IMPORTED LOGGING LOCATIONS OK!", LogEntryType.LOG_DEBUG);
            TalonLogger.WriteLogEntry("SPUN UP A NEW TALON BROKER CORRECTLY! READY TO HOOK SOME SHIT...", LogEntryType.LOG_WARN);
        }
        /// <summary>
        /// Builds a TalonSession without using logger config. This allows us to keep the same file.
        /// Use this when calling via DLL
        /// </summary>
        /// <param name="Logger"></param>
        public TalonSession(DynamicERSLogger Logger, Window MainFalconWindow, string TalonJsonLocation, bool DeepLogging = false)
        {
            // Store Logger 
            TalonLogger = Logger;
            SuperLogging = DeepLogging;
            this.OutputFolderName = Path.Combine(TalonJsonLocation, "FalconTalons");
            Directory.CreateDirectory(this.OutputFolderName);

            // Set Main Window Pointer
            this.FalconWindow = MainFalconWindow;

            // Write Information
            TalonLogger.WriteLogEntry("ERS LOGGING WAS PASSED IN TO THE CTOR OF THIS OBJECT! LOGGING SETUP COMPLETE!", LogEntryType.LOG_DEBUG);
            TalonLogger.WriteLogEntry("SPUN UP A NEW TALON BROKER CORRECTLY! READY TO HOOK SOME SHIT...", LogEntryType.LOG_WARN);
        }


        /// <summary>
        /// Gets the rectangle bound values for each window and puts them on here.
        /// </summary>
        /// <returns></returns>
        private Rectangle[] GenerateLockerShapes()
        {
            // Setup Root Window First
            TalonDissectionLocker = new WindowRelocator(this.FalconWindow);
            RootWindowLocker = new WindowRelocator(MainWindowPointer, WindowLockerPosition.OE_SCAN_APP);
            ConsoleWindowLocker = new WindowRelocator(Win32Invokers.GetConsoleWindow(), WindowLockerPosition.FALCON_CONSOLE_WINDOW);

            // Lock contents here.
            RootWindowLocker.LockWindowLocation();
            ConsoleWindowLocker.LockWindowLocation();
            TalonDissectionLocker.LockWindowLocation();

            // Return the bounds generated.
            return new Rectangle[3]
            {
                RootWindowLocker.NewWindowLocation,
                ConsoleWindowLocker.NewWindowLocation,
                TalonDissectionLocker.NewWindowLocation
            };
        }


        // ------------------- SESSION CONTROLLER INVOCATION CALLS -----------------------

        /// <summary>
        /// Boots a new talon application object and stores it into the session control.
        /// </summary>
        /// <param name="PathToApplication"></param>
        /// <returns></returns>
        public bool StartTalonSession(string PathToApplication, string AppName = "")
        {
            try
            {
                // Log Status and build window object.
                TalonLogger.WriteLogEntry($"BUILDING NEW TALON EXECUTABLE: {PathToApplication}", LogEntryType.LOG_INFO);
                if (AppName == "") { AppName = new FileInfo(PathToApplication).Name.Split('.')[0] + "_TalonWindow"; }

                // Make Window Talon, Add the contents of it into the elements list
                this.WindowTalon = new TalonWindow(AppName, PathToApplication);
                if (!WindowTalon.AppIsRunning)
                {
                    // Log failures
                    TalonLogger.WriteLogEntry($"FAILED TO LAUNCH NEW TALON WINDOW FOR EXECUTABLE: {PathToApplication}", LogEntryType.LOG_FATAL);
                    return false;
                }

                // Get Locker Setup and store root elements.
                this.GenerateLockerShapes();
                this.WindowTalon.CollectRootElements();

                // Log Passed      
                TalonLogger.WriteLogEntry($"TALON WINDOW LAUNCHED, HOOKED, AND DISSECTED CORRECTLY! THIS IS YUUUUGE", LogEntryType.LOG_WARN);
                return true;
            }
            catch (Exception ex)
            {
                // Log failures
                TalonLogger.WriteLogEntry($"FAILED TO LAUNCH NEW TALON WINDOW FOR EXECUTABLE: {PathToApplication}", LogEntryType.LOG_FATAL);
                TalonLogger.WriteLogEntry(ex, LogEntryType.LOG_FATAL, LogEntryType.LOG_ERROR);
                return false;
            }
        }
        /// <summary>
        /// Pulls up a new injector for runtime testing of code.
        /// </summary>
        public void InvokeTalonInjection()
        {

        }

        // -------------- PASSED METHOD CALLS FROM OBJECTS TO SESSION --------------------

        /// <summary>
        /// Writes specified talons to a JSON file.
        /// </summary>
        /// <param name="Elements">Elements to write (null for all)</param>
        /// <param name="CustomName">Custom file name appended if desired.</param>
        public void WriteTalonsAsJson(TalonElement[] Elements = null, string CustomName = null)
        {
            // Write elements out to json file.
            // NOTE: IF ELEMENTS IS NULL, ALL TALONS ARE WRITTEN OUT TO THE JSON FILE!
            // Write passed and print controls.
            TalonLogger.WriteLogEntry($"WRITING OUT JSON OBJECT TALONS FOR THE MAIN WINDOW TALON TO: {OutputFolderName}", LogEntryType.LOG_INFO);
            this.WindowTalon.WriteTalonsAsJson(OutputFolderName, Elements, CustomName);
        }
        /// <summary>
        /// Finds a talon on this window based on the property value provided.
        /// </summary>
        /// <param name="PropertyType">The type of lookup to perform.</param>
        /// <param name="PropertyIdentifer">The Value property identifier. Name,type string, etc</param>
        /// <param name="ValueToFind">The value to compare to.</param>
        /// <returns>A List of talons which matched the description given</returns>
        public TalonElement[] FindTalonsByProperty<TValueType>(TalonPropertyType PropertyType, string PropertyName, TValueType ValueToFind)
        {
            // Run the window call to get the values we want.
            TalonLogger.WriteLogEntry($"LOCATING FALCON TALONS ON MAIN VIEW NOW.", LogEntryType.LOG_INFO);
            return this.WindowTalon.FindTalonsByProperty<TValueType>(
                PropertyType, PropertyName, ValueToFind
            );
        }
    }
}
