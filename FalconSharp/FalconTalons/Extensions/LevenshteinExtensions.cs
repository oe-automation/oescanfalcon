﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FalconTalons.Extensions
{
    /// <summary>
    /// Supporting objects for converting/reading object values.
    /// </summary>
    public class LevenshteinExtensions
    {
        /// <summary>
        /// Gets the similarity in two strings on the main object.
        /// </summary>
        /// <param name="SourceString">Input Value</param>
        /// <param name="TargetString">Compared To</param>
        /// <returns>Int value showing the true distance between the strings.</returns>
        private static int LevenshteinDistance(string SourceString, string TargetString)
        {
            // Basic Return Cases
            if (SourceString == TargetString) return 0;
            if (SourceString.Length == 0) return TargetString.Length;
            if (SourceString.Length == 0) return TargetString.Length;

            // Vector Generation for integer distance setup
            int[] TargetVector_0 = new int[TargetString.Length + 1];
            int[] TargetVector_1 = new int[TargetString.Length + 1];

            // Setup TargetVector_0 (the previous row of distances)
            // This row is A[0][i]: Edit Travel Distance for an empty string
            // The distance is just the number of characters to delete from target
            for (int i = 0; i < TargetVector_0.Length; i++) TargetVector_0[i] = i;
            for (int i = 0; i < SourceString.Length; i++)
            {
                // The first element of TargetVector_1 is A[i+1][0]
                // Edit distance value so it's (i+1) chars from the input to match empty target
                TargetVector_1[0] = i + 1;

                // Use formula to fill in the rest of the blanks.
                for (int j = 0; j < TargetString.Length; j++)
                {
                    var cost = (SourceString[i] == TargetString[j]) ? 0 : 1;
                    TargetVector_1[j + 1] = Math.Min(TargetVector_1[j] + 1, Math.Min(TargetVector_0[j + 1] + 1, TargetVector_0[j] + cost));
                }

                // Copy TargetVector_1 (Current) to TargetVector_0 (Last) for next iteration
                for (int j = 0; j < TargetVector_0.Length; j++) TargetVector_0[j] = TargetVector_1[j];
            }

            // Return the character difference.
            return TargetVector_1[TargetString.Length];
        }

        /// <summary>
        /// Generates a double version of string likeness.
        /// </summary>
        /// <param name="SourceString">InputString</param>
        /// <param name="TargetString">TargetString</param>
        /// <returns>Double pct of lieness.</returns>
        public static double GetSimilarity(string SourceString, string TargetString)
        {
            if ((SourceString == null) || (TargetString == null)) return 0.0;
            if ((SourceString.Length == 0) || (TargetString.Length == 0)) return 0.0;
            if (SourceString == TargetString) return 1.0;

            int stepsToSame = LevenshteinDistance(SourceString, TargetString);
            return (1.0 - ((double)stepsToSame / (double)Math.Max(SourceString.Length, TargetString.Length)));
        }
    }
}
