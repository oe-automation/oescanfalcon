﻿using ERSConstructor.DynamicERS;
using FalconTalons.Extensions;
using FalconTalons.Models;
using FalconTalons.Models.Talons;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static FalconTalons.TalonSession;

namespace FalconTalons.Invokers
{
    /// <summary>
    /// Comparison extension helpers.
    /// </summary>
    public static class TalonElementExtensions
    {
        // Main types of flags to check for.
        private static BindingFlags FlagSet =  BindingFlags.Public | BindingFlags.Instance;

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the value of an invoked property on the automation window.
        /// </summary>
        /// <typeparam name="TValueType">Type of object value</typeparam>
        /// <param name="Element"></param>
        /// <param name="PropertyName">Name of property</param>
        /// <returns>Output Content</returns>
        public static TalonElementEvaluation GetAutomationMemberValue(this TalonElement Element, MemberInfo Method)
        {
            // Get the member info desired.
            var ControlType = Element.ElementBase.Current.GetType();
            var ElementMemberInfos = ControlType.GetMembers(FlagSet)
                .Where(MemberObj => MemberObj.MemberType == (MemberTypes.Field | MemberTypes.Property))
                .ToArray();

            // Get the member info desired.
            var ValueStruct = new TalonElementEvaluation(Method.Name);
            var ElementMember = (dynamic)ElementMemberInfos.FirstOrDefault(MemberObj =>
                   MemberObj.Name == Method.Name && 
                   MemberObj.MemberType == Method.MemberType
                );

            // Read Value 
            ValueStruct.PropertyValue = ElementMember.MemberType == MemberTypes.Field ?
                ((FieldInfo)ElementMember).GetValue(Element.ElementBase.Current) :
                ((PropertyInfo)ElementMember).GetValue(Element.ElementBase.Current);

            // Return object args built
            TalonLogger.WriteLogEntry($"PULLED VALUE OF {ValueStruct.PropertyValue} FROM {Element.TalonName}: {ValueStruct.PropertyValue}", LogEntryType.LOG_TRACE);
            if (!SuperLogging) { return ValueStruct; }           

            // Super Logging.
            TalonLogger.WriteLogEntry($"SPUN UP A {ValueStruct.GetType().Name} STRUCTURE FOR {ElementMember.TalonName}!", LogEntryType.LOG_TRACE);
            TalonLogger.WriteLogEntry($"STRUCT JSON:{JsonConvert.SerializeObject(ValueStruct, Formatting.None)}", LogEntryType.LOG_TRACE);
            return ValueStruct;
        }
        /// <summary>
        /// Gets the value of an invoked property on the automation window.
        /// </summary>
        /// <typeparam name="TValueType">Type of object value</typeparam>
        /// <param name="Element"></param>
        /// <param name="PropertyName">Name of property</param>
        /// <returns>Output Content</returns>
        public static TalonElementEvaluation GetAutomationMemberValue(this MemberInfo Method, TalonElement Element)
        {
            // Get the member info desired.
            var ControlType = Element.ElementBase.Current.GetType();
            var ElementMemberInfos = ControlType.GetMembers(FlagSet)
                .Where(MemberObj => MemberObj.MemberType == (MemberTypes.Field | MemberTypes.Property))
                .ToArray();

            // Get the member info desired.
            var ValueStruct = new TalonElementEvaluation(Method.Name);
            var ElementMember = (dynamic)ElementMemberInfos.FirstOrDefault(MemberObj =>
                    MemberObj.Name == Method.Name &&
                    MemberObj.MemberType == Method.MemberType
                );

            // Read Value 
            ValueStruct.PropertyValue = ElementMember.MemberType == MemberTypes.Field ?
                ((FieldInfo)ElementMember).GetValue(Element.ElementBase.Current) :
                ((PropertyInfo)ElementMember).GetValue(Element.ElementBase.Current);
 
            // Return object args built
            TalonLogger.WriteLogEntry($"PULLED VALUE OF {Element.Name} FROM {Element.TalonName}: {ValueStruct.PropertyValue}", LogEntryType.LOG_TRACE);
            if (!SuperLogging) { return ValueStruct; }

            // Super Logging.
            TalonLogger.WriteLogEntry($"SPUN UP A {ValueStruct.GetType().Name} STRUCTURE FOR {ElementMember.TalonName}!", LogEntryType.LOG_TRACE);
            TalonLogger.WriteLogEntry($"STRUCT JSON:{JsonConvert.SerializeObject(ValueStruct, Formatting.None)}", LogEntryType.LOG_TRACE);
            return ValueStruct;
        }
        /// <summary>
        /// Compares two talons to each other.
        /// Check CompareTo Properties vs Element Properties.
        /// </summary>
        /// <param name="Element">Element to compare</param>
        /// <param name="CompareTo">Element to review</param>
        /// <returns></returns>
        public static TalonSimilarity CompareTalons(this TalonElement Element, TalonElement CompareTo)
        {
            // Build Output Content
            var ElementMemberInfos = Element.GetType().GetMembers(FlagSet);
            var ValueStruct = new TalonSimilarity(Element, CompareTo);

            // Setup loop here.
            var NewResults = new Tuple<MemberInfo, double>[ElementMemberInfos.Length];
            Parallel.For(0, ElementMemberInfos.Length - 1, (MemberIndex) =>
            {
                // Start by populating all the member infos.
                var InputMemberInfo = ElementMemberInfos[MemberIndex];
                var CompareToMemberInfo = ElementMemberInfos[MemberIndex];

                // Get the value on both elements now and save them.
                object InputValue = Element.GetAutomationMemberValue(InputMemberInfo).PropertyValue;
                object CompareToValue = CompareTo.GetAutomationMemberValue(CompareToMemberInfo).PropertyValue;

                // Get next likeness object
                var Likeness = NewResults[MemberIndex] = new Tuple<MemberInfo, double>( 
                    InputMemberInfo, 
                    LevenshteinExtensions.GetSimilarity(InputValue.ToString(), CompareToValue.ToString()));
            });

            // Return the comparisons.
            ValueStruct.MemberSimilarity = NewResults;
            TalonLogger.WriteLogEntry($"FOUND {ValueStruct.Similarity}% SIMILARITY OVERALL FROM {Element.TalonName} TO {CompareTo.Name} ", LogEntryType.LOG_TRACE);
            if (!SuperLogging) { return ValueStruct; }

            // Super Logging.
            TalonLogger.WriteLogEntry($"SPUN UP A {ValueStruct.GetType().Name} STRUCTURE FOR {Element.TalonName}!", LogEntryType.LOG_TRACE);
            TalonLogger.WriteLogEntry($"STRUCT JSON:{JsonConvert.SerializeObject(ValueStruct, Formatting.None)}", LogEntryType.LOG_TRACE);
            return ValueStruct;
        }
        /// <summary>
        /// Builds a result for true/false based on if conditions work or not.
        /// </summary>
        /// <param name="Element">Element to check</param>
        /// <param name="ValueInput">CheckValues</param>
        /// <returns>Output Values made on search</returns>
        public static TalonElementSearchResult<TValueType> FindTalons<TValueType>(this TalonElement Element, TalonPropertyType PropertyType, string PropertyName, TValueType ValueToFind)
        {
            // Use ParallelOptions instance to store the CancellationToken
            CancellationTokenSource TokenSource = new CancellationTokenSource();
            ParallelOptions ParallelExecutionOptions = new ParallelOptions();
            ParallelExecutionOptions.CancellationToken = TokenSource.Token;

            // Build Value Struct now and store values on it.
            var ValueStruct = new TalonElementSearchResult<TValueType>(PropertyType, PropertyName, ValueToFind);
            TalonLogger.WriteLogEntry($"PARALLEL SEARCHING FOR TALONS ON ELEMENT PARENT {Element.TalonName}...", LogEntryType.LOG_DEBUG);
            try
            {
                // Loop Objects here.
                var ElementMemberInfos = Element.ParentTalon.GetType().GetMembers(FlagSet);
                Parallel.For(0, ElementMemberInfos.Length - 1, ParallelExecutionOptions, (MemberIndex) =>
                {
                    // Store current member. If name isn't matched keep going.
                    var ElementMember = ElementMemberInfos[MemberIndex];
                    ValueStruct.ValueFound = (TValueType)ElementMember.GetAutomationMemberValue(Element).PropertyValue;

                    // Check if the value given is the same type as the type desired.
                    ValueStruct.MemberNameMatched = ElementMember.Name == PropertyName.ToString();
                    ValueStruct.MemberValueMatched = (object)ValueStruct.ValueFound == (object)ValueToFind;
                    ValueStruct.MemberTypeMatched = typeof(TValueType) == ElementMember.DeclaringType;

                    // Check For Full pass
                    if (!ValueStruct.ConditionsSatasified) { return; }
                    TokenSource.Cancel();
                });
            }
            catch { }
            finally { TokenSource.Dispose(); }

            // Return values here.
            if (!ValueStruct.ConditionsSatasified)
            {
                TalonLogger.WriteLogEntry($"{Element.TalonName} IS MISSING ONE OR MORE REQUIRED SEARCH COMPONENTS!", LogEntryType.LOG_WARN);
                if (SuperLogging) TalonLogger.WriteLogEntry($"BUILT OBJECT: {JsonConvert.SerializeObject(ValueStruct)}", LogEntryType.LOG_TRACE);
                return ValueStruct;
            }

            // Log Passed and return value.
            TalonLogger.WriteLogEntry($"{Element.TalonName} MEETS ALL REQUIRED SEARCH CONDITIONS!", LogEntryType.LOG_INFO);
            if (SuperLogging) TalonLogger.WriteLogEntry($"BUILT OBJECT: {JsonConvert.SerializeObject(ValueStruct)}", LogEntryType.LOG_TRACE);
            return ValueStruct;
        }
    }
}
