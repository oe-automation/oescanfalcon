﻿using ERSConstructor.DynamicERS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static FalconTalons.TalonSession;

namespace FalconTalons.Invokers
{
    /// <summary>
    /// Moves a specified window around to a desired location
    /// </summary>
    public class WindowRelocator
    {
        // Window Pointer and location object 
        public IntPtr Pointer;
        public Process PointerParent;
        public System.Windows.Window DebugWindow;

        // Type of object
        private WindowLockerPosition WindowType;

        // Window location
        public Rectangle NewWindowLocation;
        public int ChildShoulderWidth = 350;
        public int ChildRegionHeight = 250;

        // Window Location Locking Setup
        private CancellationTokenSource TokenSource;

        // -----------------------------------------------------------------------------

        /// <summary>
        /// Builds new window relocator.
        /// </summary>
        /// <param name="WindowPtr"></param>
        public WindowRelocator(IntPtr WindowPtr, WindowLockerPosition WindowType)
        {
            // Task Tokens
            this.TokenSource = new CancellationTokenSource();

            // Store info
            this.WindowType = WindowType;
            TalonLogger.WriteLogEntry($"BUILDING A NEW WINDOWRELOCATOR CLASS NOW...", LogEntryType.LOG_DEBUG);

            // Store values.
            this.Pointer = WindowPtr;
            this.PointerParent = Process.GetProcesses().FirstOrDefault(ProcObj => ProcObj.MainWindowHandle == WindowPtr);
            if (this.WindowType == WindowLockerPosition.FALCON_CONSOLE_WINDOW)            
                TalonLogger.WriteLogEntry($"NOT CHECKING FOR PROCESS OBJECT FOR CONSOLE WINDOW!", LogEntryType.LOG_WARN);
            else if (PointerParent == null)
            {
                // No pointer from value given in
                TalonLogger.WriteLogEntry($"ERROR! CAN'T MOVE INVALID POINTER VALUE FOR WINDOW TYPE: {this.WindowType}", LogEntryType.LOG_ERROR);
                return;
            }

            // Store console and window locations
            this.NewWindowLocation = GenerateNewLocation(WindowType);
            this.ChildRegionHeight = this.NewWindowLocation.Height;
            this.ChildShoulderWidth = this.NewWindowLocation.Width;
            TalonLogger.WriteLogEntry($"WINDOW MOVING INVOKER BUILT FOR {this.WindowType} OK!", LogEntryType.LOG_INFO);
            TalonLogger.WriteLogEntry($"REGION SIZES: {this.ChildShoulderWidth} WIDE FOR DEBUGGER AND {this.ChildRegionHeight} FOR CONSOLE", LogEntryType.LOG_DEBUG);
            TalonLogger.WriteLogEntry($"NEW WINDOW LOCATION SET TO BE: {this.NewWindowLocation}", LogEntryType.LOG_DEBUG);
        }
        /// <summary>
        /// Builds a relocator based on the debug window.
        /// </summary>
        /// <param name="DebugWindow">Window to use </param>
        public WindowRelocator(System.Windows.Window DebugWindow)
        {
            // Task Tokens
            this.TokenSource = new CancellationTokenSource();

            // Store info
            this.DebugWindow = DebugWindow;
            this.WindowType = WindowLockerPosition.FALCON_DEBUGGER_MAIN;
            TalonLogger.WriteLogEntry($"BUILDING A NEW WINDOWRELOCATOR CLASS NOW...", LogEntryType.LOG_DEBUG);

            // Store console and window locations
            this.NewWindowLocation = GenerateNewLocation(WindowType);
            this.DebugWindow.Dispatcher.Invoke(() =>
            {
                TalonLogger.WriteLogEntry($"WINDOW MOVING INVOKER BUILT FOR {this.DebugWindow.Title} OK!", LogEntryType.LOG_INFO);
                TalonLogger.WriteLogEntry($"NEW WINDOW LOCATION SET TO BE: {this.NewWindowLocation}", LogEntryType.LOG_INFO);
            });
        }


        /// <summary>
        /// Generates a new bounds point value type on the display
        /// </summary>
        public static Rectangle GenerateNewLocation(WindowLockerPosition WindowLocation)
        {
            // Size of all displays in one rectangle
            Rectangle AllDisplayRegions = Screen.AllScreens
                .Select(screen => screen.WorkingArea)
                .Aggregate(Rectangle.Union);

            // Find display limits
            var BoundsOfAppScreen = Environment.MachineName.Contains("CDIV") ?
                Screen.GetWorkingArea(new Point(AllDisplayRegions.Right, AllDisplayRegions.Top)) :
                Screen.GetWorkingArea(new Point(AllDisplayRegions.Left, AllDisplayRegions.Top));

            // Set Withs and heights 
            int ShoulderWidth = (int)(.20 * BoundsOfAppScreen.Width);
            int ShoulderHeight = (int)(.20 * BoundsOfAppScreen.Height);

            // Apply all of this stuff to a blank rectangle here.
            int PaddingValue = 10;
            int BaseX = BoundsOfAppScreen.Location.X < 0 ? BoundsOfAppScreen.Location.X : 0;
            var NewWindowSize = new Rectangle(new Point(BaseX, 0), BoundsOfAppScreen.Size);

            // Build X And Y Start Points
            NewWindowSize.X = WindowLocation == WindowLockerPosition.FALCON_DEBUGGER_MAIN ?
                    Math.Abs(0) :
                    Math.Abs(ShoulderWidth);
            NewWindowSize.Y = WindowLocation == WindowLockerPosition.FALCON_CONSOLE_WINDOW ?
                    Math.Abs(BoundsOfAppScreen.Height - ShoulderHeight) :
                    Math.Abs(PaddingValue);

            // Build new Location. Pad window by padding value
            NewWindowSize.Width = WindowLocation == WindowLockerPosition.FALCON_DEBUGGER_MAIN ?
                ShoulderWidth + PaddingValue :
                BoundsOfAppScreen.Width - ShoulderWidth;                
            NewWindowSize.Height = WindowLocation == WindowLockerPosition.OE_SCAN_APP ?
                 BoundsOfAppScreen.Height - ShoulderHeight - PaddingValue:
                 WindowLocation == WindowLockerPosition.FALCON_DEBUGGER_MAIN ? 
                    BoundsOfAppScreen.Height - PaddingValue :
                    ShoulderHeight;

            // Restore Location value.
            NewWindowSize.Offset(BoundsOfAppScreen.Location);
            return NewWindowSize;
        }

        /// <summary>
        /// Runs a single window lock call
        /// </summary>
        private bool LockWindowOnce(Rectangle RegionsToSet)
        {
            // Update the window of the app (NOT FOR WINDOW OBJECT!)
            if (WindowType != WindowLockerPosition.FALCON_DEBUGGER_MAIN)
            {          
                // Get current size
                var MainWindowSize = new Rectangle();
                Win32Invokers.GetWindowRect(this.Pointer, ref MainWindowSize);
                if (MainWindowSize == Rectangle.Empty)
                {
                    this.TokenSource.Cancel();
                    TalonLogger.WriteLogEntry($"WINDOW {this.WindowType} APPEARS TO HAVE DISAPEARED! KILLING THREAD", LogEntryType.LOG_ERROR);
                    return false;
                }

                // Set Location using pointer values
                Win32Invokers.SetWindowPos(
                   this.Pointer, IntPtr.Zero,
                   RegionsToSet.X, RegionsToSet.Y, RegionsToSet.Width, RegionsToSet.Height,
                   Win32Invokers.SetWindowPosFlags.IgnoreZOrder
               );

                // Log Status
                if (MainWindowSize.Location == RegionsToSet.Location) { return false; }
                TalonLogger.WriteLogEntry($"MOVED WINDOW {this.WindowType} FROM {MainWindowSize} TO {RegionsToSet}", LogEntryType.LOG_TRACE);
                return true;
            }
            else
            {
                // Run this thru a dispatcher to avoid thread issues.
                this.DebugWindow.Dispatcher.Invoke(() =>
                {
                    // Convert Margins into rectangle
                    var ThicknessToRect = new Rectangle(
                        (int)this.DebugWindow.Left,
                        (int)this.DebugWindow.Top,
                        (int)(this.DebugWindow.Width),
                        (int)(this.DebugWindow.Height)
                    );

                    // Check for not shown/null size
                    if (ThicknessToRect == Rectangle.Empty)
                    {
                        this.TokenSource.Cancel();
                        TalonLogger.WriteLogEntry($"WINDOW {this.WindowType} APPEARS TO HAVE DISAPEARED! KILLING THREAD", LogEntryType.LOG_ERROR);
                        return false;
                    }

                    // Set Location using Margin values.
                    this.DebugWindow.Top = RegionsToSet.Location.Y;
                    this.DebugWindow.Left = RegionsToSet.Location.X;
                    this.DebugWindow.Width = RegionsToSet.Width;
                    this.DebugWindow.Height = RegionsToSet.Height;

                    // Log Status
                    if (ThicknessToRect.Location == RegionsToSet.Location) { return false; }
                    TalonLogger.WriteLogEntry($"MOVED {this.WindowType} FROM {this.DebugWindow.Margin} TO {RegionsToSet}", LogEntryType.LOG_TRACE);
                    return true;
                });
            }

            // Fall out false
            return false;
        }
        /// <summary>
        /// Locks the current window location to the specified point.
        /// </summary>
        public void LockWindowLocation()
        {
            // Run Async in a loop forever
            Task.Run(() =>
            {
                // Run this while not requesting cancelation
                while (!this.TokenSource.Token.IsCancellationRequested)
                {
                    // Set Value again and show it on the window
                    this.NewWindowLocation = GenerateNewLocation(this.WindowType);
                    this.ChildRegionHeight = this.NewWindowLocation.Height;
                    this.ChildShoulderWidth = this.NewWindowLocation.Width;

                    // Set Value
                    if (this.LockWindowOnce(this.NewWindowLocation)) { Thread.Sleep(100); }
                    if (TokenSource.Token.IsCancellationRequested) { break; }
                }

                // Cancel source and return.
                this.TokenSource.Cancel();
                this.TokenSource = new CancellationTokenSource();

            }, TokenSource.Token);
        }

        /// <summary>
        /// Stops the cancelation token source and keeps it in local storage.
        /// </summary>
        public void UnlockWindow()
        {
            // Throw the token and return.
            this.TokenSource.Cancel();
            this.TokenSource = new CancellationTokenSource();
        }
    }
}
