﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;
using System.Windows.Forms;
using System.Windows.Interop;
using ERSConstructor.DynamicERS;
using FalconTalons.Invokers;
using FalconTalons.ViewGeneration;
using Newtonsoft.Json;

namespace FalconTalons.Models.Talons
{
    /// <summary>
    /// Talon Window object. 
    /// Used to generate a window and it's controls as talons.
    /// </summary>
    public class TalonWindow : Talon
    {
        // Process values
        public TalonElement RootWindowTalon;
        public TalonExecutable WindowExecutable;
        public TalonElement[] WindowElements => RootWindowTalon?.ChildTalons;

        // Other Useful values for this application
        public Process AppProcess => WindowExecutable?.AppProcess;
        public string ApplicationName => WindowExecutable?.AppName;
        public bool AppIsRunning => WindowExecutable == null ? false : WindowExecutable.AppIsRunning;
        public IntPtr ProcessPointer => AppProcess == null ? IntPtr.Zero : AppProcess.Handle;
        public IntPtr MainWindowPointer => AppProcess == null ? IntPtr.Zero : AppProcess.MainWindowHandle;

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Builds a new talon object
        /// </summary>
        /// <param name="Name"></param>
        public TalonWindow(string Name) : base(Name)
        {
            // Set Talon Type
            this.TypeOfTalon = TalonType.WINDOW_TALON;
        }
        /// <summary>
        /// Builds a talon with a base element
        /// </summary>
        /// <param name="Name">Talon Name</param>
        /// <param name="AppToInvoke">Base element.</param>
        internal TalonWindow(string Name, string AppToInvoke) : base(Name)
        {
            // Set Talon Type
            this.TypeOfTalon = TalonType.WINDOW_TALON;

            // Store values and launch/hook an existing or new process
            this.WindowExecutable = new TalonExecutable(AppToInvoke);
            if (this.WindowExecutable.AppPointer == IntPtr.Zero) { return; }

            // Set Main Window and Children
            this.AppToInvoke = AppToInvoke;
            this.ElementBase = AutomationElement.FromHandle(this.MainWindowPointer);
        }

        
        /// <summary>
        /// Gathers all the new talon element root objects for this window.
        /// </summary>
        internal void CollectRootElements()
        {
            // Time This
            var InitTime = new Stopwatch(); InitTime.Start();
            TalonSession.TalonLogger.WriteLogEntry($"BUILDING TALON WINDOW OBJECT: {this.TalonName}", LogEntryType.LOG_INFO);
            this.RootWindowTalon = new TalonElement(this.TalonName, this, this.ElementBase);
            TalonSession.TalonLogger.WriteLogEntry($"DONE BUILDING TALON WINDOW OBJECT: {this.TalonName}! TIME PASSED: {InitTime.Elapsed.ToString("g")}", LogEntryType.LOG_INFO);
            TalonSession.TalonLogger.WriteLogEntry($"WINDOW TALON OBJECT IS BEING LOCKED NOW! STOP IT BEFORE TRYING TO MOVE IT", LogEntryType.LOG_INFO);
        }

        /// <summary>
        /// Talons out to Json.
        /// </summary>
        /// <param name="TalonsToWrite">Talons to write out.</param>
        internal void WriteTalonsAsJson(string OutputFolderName, TalonElement[] Elements = null, string CustomName = null)
        {
            // Check elements value for null
            if (Elements == null) { Elements = this.WindowElements; }

            // Build File Name 
            string FileName = $"{this.ApplicationName}_{(CustomName == null ? $"PID-{this.AppProcess.Id}" : CustomName)}_{DateTime.Now.ToString("hh-mm-ss_mm-dd-yyyy")}.json";
            TalonSession.TalonLogger.WriteLogEntry($"WRITING JSON OUTPUT FOR TALONS TO: {FileName}", LogEntryType.LOG_DEBUG);
            string OutputFile = Path.Combine(OutputFolderName, FileName);

            // Write Output to file.
            string JsonTalons = JsonConvert.SerializeObject(Elements);
            File.WriteAllText(OutputFile, JsonTalons);
        }

        /// <summary>
        /// Finds a talon on this window based on the property value provided.
        /// </summary>
        /// <param name="PropertyType">The type of lookup to perform.</param>
        /// <param name="PropertyIdentifer">The Value property identifier. Name,type string, etc</param>
        /// <param name="ValueToFind">The value to compare to.</param>
        /// <returns></returns>
        internal TalonElement[] FindTalonsByProperty<TValueType>(TalonPropertyType PropertyType, string PropertyName, TValueType ValueToFind)
        {
            // Loop on all the element items in parallel.
            List<TalonElement> MatchedElements = new List<TalonElement>();
            Parallel.For(0, this.WindowElements.Length - 1, (ElementIndex) =>
            {
                // Store current element and loop member values of it.
                var CurrentElement = this.WindowElements[ElementIndex];
                var ElementProperties = CurrentElement.AsJsonArray;

                // Loop the member infos here. Check Values accordingly.
                var ElementMemberInfos = typeof(TalonElement).GetMembers();
                Parallel.For(0, ElementMemberInfos.Length - 1, (MemberIndex) =>
                {
                    // Store current member info and then cast as needed.
                    var ElementMember = ElementMemberInfos[MemberIndex];
                    object ElementMemberValue = ElementMember.GetAutomationMemberValue(CurrentElement);
                    if (ElementMemberValue == null) { return; }

                    // Check if the value given is the same type as the type desired.
                    var TalonsFound = CurrentElement.FindTalons(PropertyType, PropertyName, ValueToFind);
                    bool MemberNameMatched = ElementMember.Name == PropertyName.ToString();
                    bool MemberValueMatched = ElementMemberValue == (object)ValueToFind;
                    bool MemberTypeMatches = typeof(TValueType) == ElementMember.DeclaringType;
                });
            });  

            // Return a temp empty talon set.
            return new TalonElement[0] { };
        }
    }
}
