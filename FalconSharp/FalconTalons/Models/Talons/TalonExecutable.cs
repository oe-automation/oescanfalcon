﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;
using System.Windows.Forms;
using ERSConstructor.DynamicERS;
using Newtonsoft.Json;

namespace FalconTalons.Models.Talons
{
    /// <summary>
    /// Executable object for a falcon app
    /// </summary>
    public class TalonExecutable
    {
        // Process Information
        public bool AppIsRunning
        {
            get
            {
                try
                {
                    // Show Start time and pass
                    var WindowTitle = this.AppProcess.MainWindowTitle;
                    if (string.IsNullOrEmpty(this.AppProcess.MainWindowTitle))
                    {
                        // Log Dead and return failed.
                        TalonSession.TalonLogger.WriteLogEntry(
                            $"PROCESS OBJECT IS NOT YET ALIVE LIVE FOR {AppProcess.ProcessName}! ",
                            LogEntryType.LOG_TRACE
                        );
                        return false;
                    }

                    // Log Alive return passed
                    TalonSession.TalonLogger.WriteLogEntry(
                        $"PROCESS OBJECT IS STILL LIVE FOR {AppProcess.ProcessName}! " +
                        $"MAIN WINDOW NAME: {WindowTitle}", 
                        LogEntryType.LOG_TRACE
                    );
                    return true;
                }
                catch (Exception ex)
                {
                    // Failure logging
                    TalonSession.TalonLogger.WriteLogEntry($"PROCESS OBJECT COULD NOT RETURN A WINDOW NAME!", LogEntryType.LOG_ERROR);
                    TalonSession.TalonLogger.WriteLogEntry(ex, LogEntryType.LOG_ERROR, LogEntryType.LOG_TRACE);
                    return false;
                }
            }
        }     // Bool to show if the process is running or not.
        public Process AppProcess;      // Process used to boot this app
        public string AppExePath;       // Path to the app exe
        public IntPtr AppPointer
        {
            get
            {
                // Look for not running/no process
                if (AppProcess == null) { return IntPtr.Zero; }
                return !AppIsRunning ? IntPtr.Zero : AppProcess.MainWindowHandle;
            }
        }     // Pointer of the window.
        public string AppName
        {
            get
            {
                // Look for not running/no process
                if (AppProcess == null) { return string.Empty; }
                return !AppIsRunning ? string.Empty : AppProcess.ProcessName;
            }
        }        // Name of running process.

        // Output And Error Lists
        internal List<string> OutputContent = new List<string>();
        internal List<string> ErrorContent = new List<string>();

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Builds a new falcon talon process.
        /// </summary>
        /// <param name="AppToInvoke"></param>
        public TalonExecutable(string AppToInvoke)
        {
            // Log Status
            this.AppExePath = AppToInvoke;
            TalonSession.TalonLogger.WriteLogEntry("CREATING NEW TALON EXECUTABLE NOW...", LogEntryType.LOG_WARN);
            TalonSession.TalonLogger.WriteLogEntry($"INVOKING (OR CONSUMING) EXECUTABLE OBJECT {AppExePath}...", LogEntryType.LOG_DEBUG);

            // Look for existing output
            if (!this.StartNewProcess(AppToInvoke)) { TalonSession.TalonLogger.WriteLogEntry($"FAILED TO LAUNCH NEW EXECUTABLE!", LogEntryType.LOG_FATAL); return; }
            TalonSession.TalonLogger.WriteLogEntry($"LAUNCHED NEW EXECUTABLE CORRECTLY!", LogEntryType.LOG_WARN);

            // Write satus for a failure.
            if (!AppIsRunning) TalonSession.TalonLogger.WriteLogEntry($"APP STATUS IS NOT ACTIVE RUNNING? THIS IS STRANGE", LogEntryType.LOG_WARN); 
        }

        /// <summary>
        /// Generates a new process object for a falcon exec
        /// </summary>
        /// <param name="AppToInvoke">App to run</param>
        private bool StartNewProcess(string AppToInvoke)
        {
            // Make process and set file object
            TalonSession.TalonLogger.WriteLogEntry($"DYNAMIC PROCESS SELECTION STARTING NOW. LOCATING EXISTING WINDOWS TO USE", LogEntryType.LOG_INFO);

            // Check for exsiting.
            string[] NameNoExtArray = new FileInfo(AppToInvoke).Name.Split('.');
            string AppProcName = string.Join(".", NameNoExtArray.Take(NameNoExtArray.Length - 1).ToArray());
            var ProcsFound = Process.GetProcesses()
                .Where(ProcObj =>
                    ProcObj.MainWindowTitle == AppProcName ||
                    ProcObj.ProcessName.Contains(AppProcName))
                .ToList();

            // Log Existing processes.
            if (ProcsFound.Count != 0)
            {
                TalonSession.TalonLogger.WriteLogEntry($"LOCATED A TOTAL OF {ProcsFound.Count} EXISTING PROCESSES THAT MAY BE WHAT WE WANT", LogEntryType.LOG_INFO);
                TalonSession.TalonLogger.WriteLogEntry($"TRYING TO HOOK THE FIRST PROCESS OBJECT FOUND...", LogEntryType.LOG_DEBUG);
                this.AppProcess = ProcsFound.FirstOrDefault();

                // Write info and return.
                if (this.AppProcess.HasExited == true || this.AppProcess.StartTime == null)
                {
                    // Failed. Log, kill, and make new.
                    TalonSession.TalonLogger.WriteLogEntry("ERROR COULD NOT USE THIS PROCESS OBJECT!", LogEntryType.LOG_ERROR);
                    TalonSession.TalonLogger.WriteLogEntry("BOOTING NEW PROCESS FROM SCRATCH AND KILLING THE OLD ONE...", LogEntryType.LOG_ERROR);

                    // Make new and kill old.
                    this.AppProcess.Kill();
                    this.AppProcess = new Process() { StartInfo = new ProcessStartInfo(AppToInvoke) };
                }
                else
                {
                    // Passed
                    TalonSession.TalonLogger.WriteLogEntry("PROCESS APPEARS TO BE A VALID INSTANCE OF THE DESIRED APP!", LogEntryType.LOG_INFO);
                    TalonSession.TalonLogger.WriteLogEntry("STORING IT AND USING THIS PROCESS FOR THE REMAINDER OF EXECUTION", LogEntryType.LOG_DEBUG);
                    this.WriteProcessInfo();
                    return true;
                }
            }

            // Log Process info.
            this.AppProcess = new Process() { StartInfo = new ProcessStartInfo(AppToInvoke) };
            TalonSession.TalonLogger.WriteLogEntry("NO EXISTING PROCESS OBJECTS WERE LOCATED OR COULD BE USED!", LogEntryType.LOG_WARN);
            TalonSession.TalonLogger.WriteLogEntry("TALON PROCESS OBJECT CREATED OK!", LogEntryType.LOG_DEBUG);

            // Modify output redirects
            try
            {
                // Now modify the process so it redirects output.
                this.AppProcess.StartInfo.UseShellExecute = false;
                this.AppProcess.StartInfo.RedirectStandardError = true;
                this.AppProcess.StartInfo.RedirectStandardOutput = true;
                this.AppProcess.ErrorDataReceived += (sender, args) => { if (args.Data != null) { this.ErrorContent.Add(args.Data.TrimEnd()); }; };
                this.AppProcess.OutputDataReceived += (sender, args) => { if (args.Data != null) { this.OutputContent.Add(args.Data.TrimEnd()); }; };
                TalonSession.TalonLogger.WriteLogEntry("TALON PROCESS OUTPUT REDIRECTION AND RUNAS SETUP OK!", LogEntryType.LOG_DEBUG);
            }
            catch (Exception ex)
            {
                // Log what failed here.
                TalonSession.TalonLogger.WriteLogEntry($"COULD NOT MODIFY EXISTING PROCESS TO REDIRECT OUTPUT!", LogEntryType.LOG_ERROR);
                TalonSession.TalonLogger.WriteLogEntry("THIS IS NOT A MASSIVE ISSUE, BUT CAN MAKE DEBUGGING HARDER", LogEntryType.LOG_ERROR);
                TalonSession.TalonLogger.WriteLogEntry(ex, LogEntryType.LOG_WARN, LogEntryType.LOG_TRACE);
            }

            // Launch the process
            try
            {
                // Start the process
                int TimePassed = 0;
                this.AppProcess.Start();
                TalonSession.TalonLogger.WriteLogEntry($"PROCESS LAUNCHED. WAITING 30 SECONDS FOR OPENING IDLE STATUS...", LogEntryType.LOG_DEBUG);
                while (string.IsNullOrEmpty(this.AppProcess.MainWindowTitle) && TimePassed <= 30000)
                {
                    // Log info about waiting.
                    if (TimePassed % 2000 == 0) 
                        TalonSession.TalonLogger.WriteLogEntry($"STILL WAITING FOR MAIN WINDOW. TIME PASSED {TimePassed}ms", LogEntryType.LOG_TRACE);
                  
                    // Tick sleep timer and wait.
                    TimePassed += 100;
                    System.Threading.Thread.Sleep(100);
                    this.AppProcess.Refresh();
                }

                // Write some start info.
                this.WriteProcessInfo(); 
                return true;
            }
            catch (Exception ex)
            {
                // Log Failure
                TalonSession.TalonLogger.WriteLogEntry($"FAILED TO LAUNCH NEW TALON APPLICATION!", LogEntryType.LOG_FATAL);
                TalonSession.TalonLogger.WriteLogEntry(ex, LogEntryType.LOG_FATAL, LogEntryType.LOG_FATAL);
                return false;   
            }
        }
        /// <summary>
        /// Prints out the info about a newly configured process.
        /// </summary>
        private void WriteProcessInfo()
        {
            // Print info. Log Failure
            try
            {
                // Once Waited and Open Log Info
                TalonSession.TalonLogger.WriteLogEntry($"PROCESS OBJECT HAS BEEN STARTED! TIME OF LAUNCH {AppProcess.StartTime.ToString("g")}", LogEntryType.LOG_INFO);
                TalonSession.TalonLogger.WriteLogEntry($"PROCESS ID:   {this.AppProcess.Id}", LogEntryType.LOG_DEBUG);
                TalonSession.TalonLogger.WriteLogEntry($"PROCESS NAME: {this.AppProcess.ProcessName}", LogEntryType.LOG_DEBUG);
                TalonSession.TalonLogger.WriteLogEntry($"PROCESS PATH: {this.AppProcess.StartInfo.FileName}", LogEntryType.LOG_DEBUG);
                TalonSession.TalonLogger.WriteLogEntry($"PROCESS PTR:  {this.AppProcess.MainWindowHandle}", LogEntryType.LOG_DEBUG);
                TalonSession.TalonLogger.WriteLogEntry($"WINDOW TITLE: {this.AppProcess.MainWindowTitle}", LogEntryType.LOG_DEBUG);
            }
            catch (Exception ex)
            {
                // Log Failure
                TalonSession.TalonLogger.WriteLogEntry($"FAILED TO LAUNCH NEW TALON APPLICATION!", LogEntryType.LOG_FATAL);
                TalonSession.TalonLogger.WriteLogEntry(ex, LogEntryType.LOG_FATAL, LogEntryType.LOG_FATAL);
            }
        }
    }
}
