﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;
using ERSConstructor.DynamicERS;
using FalconTalons.Extensions;
using Newtonsoft.Json;
using static FalconTalons.TalonSession;

namespace FalconTalons.Models.Talons
{
    /// <summary>
    /// Base object of a talon used to find control values
    /// </summary>
    public class Talon
    {
        // Identifiers
        public string TalonName;        // Name of talon
        public string AppToInvoke;      // Path to the running app
        public TalonType TypeOfTalon;   // Type of talon

        // Automation Elements
        private AutomationElement _elementBase;
        private AutomationElement[] _elementChildren;

        [JsonIgnore]
        public AutomationElement ElementBase
        {
            get => _elementBase;
            set
            {
                // Store value and get children controls.
                this._elementBase = value;
                TalonLogger.WriteLogEntry($"BASE ELEMENT {this.TalonName} HAS BEEN UPDATED!", LogEntryType.LOG_TRACE);

                // Update Children here
                try
                {
                    // Get Child Values
                    var Children = ElementBase.FindAll(TreeScope.Subtree, Condition.TrueCondition);
                    this._elementChildren = new AutomationElement[Children.Count];
                    Children.CopyTo(this.ElementChildren, 0);

                    // Get all elements with valid names that don't appear twice and are not the base item.
                    this._elementChildren = this._elementChildren
                        .GroupBy(ElemObj => ElemObj.Current.BoundingRectangle)
                        .Select(ElemObj => ElemObj.First())
                        .Where(ElemObj =>
                            ElemObj.Current.Name != "N/A" &&
                            ElemObj.Current.Name != "" &&
                            ElemObj.Current.AutomationId != "" &&
                            ElemObj != ElementBase)
                        .ToArray();

                    // Return array
                    TalonLogger.WriteLogEntry($"UPDATED CHILD CONTROLS FOR INPUT CONTROL: {TalonName} OK! (TOTAL OF {_elementChildren.Length} CONTROLS FOUND)", LogEntryType.LOG_TRACE);
                }
                catch (Exception ex)
                {
                    // Log the failure and return nothing.
                    TalonLogger.WriteLogEntry($"FAILED TO UPDATE CHILD CONTROLS FOR INPUT CONTROL: {TalonName}", LogEntryType.LOG_ERROR);
                    TalonLogger.WriteLogEntry(ex);
                }
            }
        }             // Main Element of this object 
        [JsonIgnore]
        public AutomationElement[] ElementChildren
        {
            set => _elementChildren = value;
            get
            {
                // Check null amd Return
                return ElementBase != null ? _elementChildren : new AutomationElement[] { };
            }
        }       // Contents/Sub content of the object

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Builds a new talon object
        /// </summary>
        /// <param name="Name"></param>
        public Talon(string Name)
        {
            // Store name and type of talon and write out some info
            this.TalonName = Name;

            // Write out info if super logging is on
            if (!SuperLogging) { return; }
            TalonLogger.WriteLogEntry($"TALON OBJECT {Name} HAS BEEN BUILT OK!", LogEntryType.LOG_DEBUG);
            TalonLogger.WriteLogEntry($"TALON TYPE: {TypeOfTalon}", LogEntryType.LOG_TRACE);
            TalonLogger.WriteLogEntry($"INVOKES APPLICATION: {AppToInvoke}", LogEntryType.LOG_TRACE);

            // Add Event Helper
            TalonLogger.WriteLogEntry($"SETUP EVENT HANDLER FOR TALON OK!", LogEntryType.LOG_TRACE);
        }
    }
}
