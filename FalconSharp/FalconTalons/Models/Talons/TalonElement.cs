﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation;
using ERSConstructor.DynamicERS;
using FalconTalons.ViewGeneration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using FalconTalons.Invokers;
using static FalconTalons.TalonSession;
using FalconTalons.Extensions;

namespace FalconTalons.Models.Talons
{
    /// <summary>
    /// Falcon Talon object
    /// </summary>
    public class TalonElement : Talon
    {
        [JsonIgnore]
        public TalonWindow ParentWindow;        // Window parent        
        [JsonIgnore]
        public TalonElement ParentTalon;        // Parent of this element

        // Element Properties (Ordered like this to clean up json output)
        public string Name=> ElementBase?.Current.Name?? ElementBase?.Current.Name;
        public string ClassName => ElementBase?.Current.ClassName?? ElementBase?.Current.Name;
        public string AutomationId => ElementBase?.Current.AutomationId?? ElementBase?.Current.AutomationId;
        public string FrameworkId => ElementBase?.Current.FrameworkId?? ElementBase?.Current.FrameworkId;
        public int ProcessId => ElementBase != null ? ElementBase.Current.ProcessId : -1;
        public bool IsEnabled => ElementBase != null ? ElementBase.Current.IsEnabled : false;
        public bool IsContentElement => ElementBase != null ? ElementBase.Current.IsContentElement : false;
        public bool IsControlElement => ElementBase != null ? ElementBase.Current.IsControlElement : false;
        public ControlType ControlType => ElementBase?.Current.ControlType?? ElementBase?.Current.ControlType;
        public Rect BoundingRectangle => ElementBase != null ? ElementBase.Current.BoundingRectangle : new Rect();
        public IntPtr MainWindowPointer => ParentWindow != null ? ParentWindow.MainWindowPointer : ParentTalon.MainWindowPointer;
        public IntPtr ProcessPointer => ParentWindow != null ? ParentWindow.ProcessPointer : ParentTalon.ProcessPointer;

        // JSON Array properties from the control.
        [JsonIgnore]
        public Tuple<string,object>[] AsJsonArray =>            
            this.GetType().GetMembers()
                .Select(MemberObj => MemberObj.GetAutomationMemberValue(this))
                .GroupBy(ItemValue => ItemValue.MemberTuple).First()
                .Select(TupleObjs => TupleObjs.MemberTuple)
                .ToArray();

        // Children of the base talon object
        private TalonElement[] _childTalons;
        public TalonElement[] ChildTalons
        {
            get => this._childTalons == null ? this.FindChildTalons() : this._childTalons;
            set => _childTalons = value;
        }

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Builds a new talon object
        /// </summary>
        /// <param name="Name"></param>
        public TalonElement(string Name) : base(Name)
        {
            // Set Talon Type
            this.TypeOfTalon = TalonType.ELEMENT_TALON;
        }
        /// Builds a talon with a base element
        /// </summary>
        /// <param name="Name">Talon Name</param>
        /// <param name="ParentTalon">Base element.</param>
        /// <param name="Element">Element to control</param>
        public TalonElement(string Name, TalonElement ParentTalon, AutomationElement WindowElement) : base(Name)
        {
            // Store Parent and Type
            this.ParentTalon = ParentTalon;
            this.ElementBase = WindowElement;
            this.AppToInvoke = ParentTalon.AppToInvoke;
            this.TypeOfTalon = TalonType.ELEMENT_TALON;
            this.ParentWindow = ParentTalon.ParentWindow;

            // Store Element Content
            this.TalonName =
                $"Talon_{(this.ElementBase.Current.Name == "N/A" ? "NOT-SET" : this.ElementBase.Current.Name)}" +
                $"{(this.AutomationId == "" ? "_NO-ID" : $"_{this.AutomationId}")}".Replace(" ", "-");

            // Clone Properties
            this.ChildTalons = this.FindChildTalons();
        }
        /// Builds a talon with a base element
        /// </summary>
        /// <param name="Name">Talon Name</param>
        /// <param name="ParentTalon">Base element.</param>
        /// <param name="Element">Element to control</param>
        public TalonElement(string Name, TalonWindow ParentWindow, AutomationElement WindowElement) : base(Name)
        {
            // Store Parent and Type
            this.ElementBase = WindowElement;
            this.ParentWindow = ParentWindow;
            this.TypeOfTalon = TalonType.ELEMENT_TALON;
            this.AppToInvoke = ParentWindow.AppToInvoke;

            // Store Element Content
            this.TalonName =
                ($"Talon_{(this.ElementBase.Current.Name == "N/A" ? "NO-NAME" : this.ElementBase.Current.Name)}_" +
                $"{(this.ElementBase.Current.AutomationId == "" ? "NO-ID" : $"{this.AutomationId}")}")
                .Replace(" ", "-");

            // Clone Properties
            this.ChildTalons = this.FindChildTalons();
        }


        /// <summary>
        /// Builds the talons of the window children and dumps them back
        /// </summary>
        /// <returns>Array of talons on the window.</returns>
        public TalonElement[] FindChildTalons()
        {
            // Check for not child elements.
            if (this.ElementChildren.Length == 0) { return new TalonElement[0] { }; }

            // Get Talons list.
            List<TalonElement> TalonsBuilt = new List<TalonElement>();
            TalonLogger.WriteLogEntry($"BUILDING CHILD ELEMENTS AS TALONS FOR TALON {this.TalonName}", LogEntryType.LOG_INFO);
            TalonLogger.WriteLogEntry($"{this.ElementChildren.Length} CHILD ELEMENTS", LogEntryType.LOG_DEBUG);

            // Loop the elements and store values.
            Parallel.For(0, ElementChildren.Length - 1, (TalonIndex) =>
            {
                // Build and store element. Skip if element is not real
                AutomationElement ChildElem = ElementChildren[TalonIndex];
                if (ChildElem == null || ChildElem.Current.ProcessId == 0) { return; }

                try
                {
                    // Name Control, build new talon and add to output.
                    string NameOfControl = $"{ChildElem.Current.Name}_{(this.AutomationId == "" ? "NOID" : $"{this.AutomationId}")}".Replace(" ", "-");
                    var NextObject = new TalonElement(NameOfControl, this, ChildElem);

                    // If we want to do outlining do it here. (Tied to superlogging)
                    if (SuperLogging)
                    {
                        TalonLogger.WriteLogEntry($"GENERATING OUTLINE OF {this.TalonName} ELEMENT NOW", LogEntryType.LOG_DEBUG);
                        TalonPainter ElementPainter = new TalonPainter(this);
                        ElementPainter.ShowControlOutline();
                    }

                    // Add to list of objects
                    TalonsBuilt.Add(NextObject);

                    // Build log string and write output.
                    string LogString = $"ELEMENT GENERATED [{TalonIndex}]: {NameOfControl} | TYPE: {ChildElem.Current.ControlType.LocalizedControlType}";
                    TalonLogger.WriteLogEntry($"{LogString}", LogEntryType.LOG_INFO);
                    if (!SuperLogging) { return; }

                    // Write Trace Logging.
                    TalonLogger.WriteLogEntry($"{JsonConvert.SerializeObject(NextObject, Formatting.None)}", LogEntryType.LOG_TRACE);
                }
                catch (Exception Ex)
                {
                    TalonLogger.WriteLogEntry($"REQUESTED ELEMENT COULD NOT BE LOCATED EVEN AFTER BEING A LOCATED!", LogEntryType.LOG_ERROR);
                    TalonLogger.WriteLogEntry(Ex);
                }
            });       

            // Return the tokens.
            TalonLogger.WriteLogEntry($"RETURNING {TalonsBuilt.Count} CHILD ELEMENTS GENERATED FOR {this.TalonName} NOW", LogEntryType.LOG_INFO);
            return TalonsBuilt.ToArray();
        }
    }
}
