﻿using ERSConstructor.DynamicERS;
using FalconTalons.Models.Talons;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static FalconTalons.TalonSession;

namespace FalconTalons
{
    /// <summary>
    /// Struct containing params to pull a property value and then returning it.
    /// </summary>
    public struct TalonElementEvaluation
    {
        // Setup Values
        public string PropertyName;
        public object PropertyValue;

        // Output Tuple and Object
        public Tuple<string, object> MemberTuple =>
            new Tuple<string, object>(this.PropertyName, this.PropertyValue);

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Builds talon eval struct arguments.
        /// </summary>
        /// <param name="PropertyName">Name of property to get.</param>
        public TalonElementEvaluation(string PropertyName)
        {
            // Store input and save
            this.PropertyName = PropertyName;
            this.PropertyValue = null;
        }
    }
    /// <summary>
    /// Value Evaluation setup
    /// </summary>
    public struct TalonElementSearchResult<TValueType>
    {
        // Input Values
        public double Tolerance;
        public string PropertyName;
        public TValueType ValueToFind;
        public TValueType ValueFound;
        public TalonPropertyType PropertyType;

        // Bools to set if matched or not.
        public bool ConditionsSatasified => this.MemberNameMatched && this.MemberValueMatched && this.MemberTypeMatched;
        public bool MemberNameMatched;
        public bool MemberValueMatched;
        public bool MemberTypeMatched;

        // To String Conversion. Bools in order of (NAME, VALUE, TYPE)
        public override string ToString() =>
            $"[ {MemberNameMatched.ToString().ToUpper()}, " +
            $"{MemberNameMatched.ToString().ToUpper()}, " +
            $"{MemberNameMatched.ToString().ToUpper()} ]";

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Builds a new value comparison arg object.
        /// </summary>
        public TalonElementSearchResult(TalonPropertyType PropertyType, string PropertyName, TValueType ValueToFind, double Tolerance = 0.00)
        {
            // Store inputs
            this.Tolerance = Tolerance;
            this.ValueToFind = ValueToFind;
            this.PropertyName = PropertyName;
            this.PropertyType = PropertyType;

            // Default all bools to false.
            this.MemberNameMatched = false;
            this.MemberValueMatched = false;
            this.MemberTypeMatched = false;

            // Set Output to false.
            this.ValueFound = default;
        }
    }
    /// <summary>
    /// Shows how alike two talons are by comparing their values.
    /// Prop values are weighted.
    /// </summary>
    public struct TalonSimilarity
    {
        // Comparison values.
        public Tuple<MemberInfo, double>[] MemberSimilarity;
        public double Similarity => (this.MemberSimilarity.Select(TupleVal => TupleVal.Item2).Sum()) / this.MemberSimilarity.Length;

        // Elements to compare.
        public TalonElement InputElement;
        public TalonElement Comparator;

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Generates args for comparison.
        /// </summary>
        /// <param name="InputElement">Compare this element</param>
        /// <param name="Comparator">To This Element.</param>
        public TalonSimilarity(TalonElement InputElement, TalonElement Comparator)
        {
            // Store values.
            this.InputElement = InputElement;
            this.Comparator = Comparator;

            // Set start likeness.
            this.MemberSimilarity = new Tuple<MemberInfo, double>[0] { };
        }
    }
}
