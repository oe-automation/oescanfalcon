﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FalconTalons
{
    /// <summary>
    /// Types of talon for control finding.
    /// </summary>
    public enum TalonType
    {
        BASE_TALON,
        WINDOW_TALON,
        ELEMENT_TALON,
    }

    /// <summary>
    /// Use this to find talons on a window using the find elements call.
    /// </summary>
    [Flags]
    public enum TalonPropertyType : uint
    {
        // Base Property Types
        COMPLETE_SEARCH = 0x00000000,
        PROPERTY_NAME = 0x00000001,
        PROPERTY_TYPE = 0x00000002,
        PROPERTY_VALUE = 0x00000003,

        // Toggles for search conditions 
        INCLUDE_CHILDREN = 0x00000010,
    }

    /// <summary>
    /// Flags to set locations of elements on locker
    /// </summary>
    public enum WindowLockerPosition
    {
        // NONE
        FALCON_CONSOLE_WINDOW = 0x00,
        FALCON_DEBUGGER_MAIN = 0x01,
        OE_SCAN_APP = 0x02,
    }
}
