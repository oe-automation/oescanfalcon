﻿using IronPython.Hosting;
using IronPython.Runtime;
using IronPython;
using Microsoft.Scripting.Hosting;
using Microsoft.Scripting;
using ERSConstructor.DynamicERS;

namespace FalconPyEngine
{
    /// <summary>
    /// Python invoker for a C# Execution setup
    /// </summary>
    public class IronPyEngine
    {
        // Python engine items.
        public ScriptEngine PyEngine;       // Python engine for method execution
        public dynamic PyScope;             // Scope for the python engine to control.

        // Logger object
        private static bool SuperLog;                  // Changes json view output.
        private static DynamicERSLogger PyLogger;      // Logger object for python scripts

        // -----------------------------------------------------------------------------

        /// <summary>
        /// Builds a new python engine for C# invocation
        /// </summary>
        public IronPyEngine(DynamicERSLogger Logger, bool SuperLogging = false)
        {
            // Build an engine and a scope.
            SuperLog = SuperLogging;
            PyEngine = Python.CreateEngine();
            PyScope = PyEngine.CreateScope();

            // Build Logger and print info about the python engine.
            PyLogger = Logger;
            PyLogger.WriteLogEntry($"PYENGINE HAS BEEN CONFIGURED OK!", LogEntryType.LOG_INFO);
            PyLogger.WriteLogEntry($"PYTHON VERSION: {PyEngine.LanguageVersion}", LogEntryType.LOG_DEBUG);
        }

        /// <summary>
        /// Adds a list of path values into the search path for the current engine.
        /// </summary>
        /// <param name="PathsToAppend">Paths to add into the engine.</param>
        public void AddSearchPaths(string PathsToAppend)
        {
            // Add in as an array now.
            AddSearchPaths(new string[] { PathsToAppend });
        }
        /// <summary>
        /// Adds a list of path values into the search path for the current engine.
        /// </summary>
        /// <param name="PathsToAppend">Paths to add into the engine.</param>
        public void AddSearchPaths(string[] PathsToAppend)
        {
            // Get the current paths, and add new ones into the python path
            var CurrentPaths = PyEngine.GetSearchPaths();
            PyLogger.WriteLogEntry($"ADDING PATHS TO FALCON'S PYTHON ENGINE", LogEntryType.LOG_INFO);
            PyLogger.WriteLogEntry($"CURRENT ENGINE PATH COUNT: {CurrentPaths.Count}", LogEntryType.LOG_TRACE);
            PyLogger.WriteLogEntry($"PATHS TO APPEND INTO ENGINE: {PathsToAppend.Length}", LogEntryType.LOG_TRACE);

            // Loop each one and append it into the paths
            foreach (var PathValue in PathsToAppend)
            {
                CurrentPaths.Add(PathValue);
                PyLogger.WriteLogEntry($"--> APPENDED PATH: {PathValue}", LogEntryType.LOG_TRACE);
            }

            // Log info about path count.
            PyLogger.WriteLogEntry($"APPENDED NEW PATHS INTO ENGINE AND APPLIED THEM. TOTAL PATHS {CurrentPaths.Count}", LogEntryType.LOG_DEBUG);
            PyEngine.SetSearchPaths(CurrentPaths);
        }
    }
}
