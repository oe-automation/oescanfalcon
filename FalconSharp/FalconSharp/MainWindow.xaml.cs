﻿using ERSConstructor.DynamicERS;
using FalconSharp.FalconConstants;
using FalconTalons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FalconSharp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class FalconSharpMainWindow : Window
    {
        public FalconSharpMainWindow()
        {   
            // Launch Falcon           
            if (!FalconStore.SetupConstants()) { return; }
            FalconStore.SetupScanType();

            // Start Mainwindow
            InitializeComponent();
            FalconStore.MainWindow = this;

            // Run Falcon Setup while the main window opens up
            Task.Run(() => { InvokeFalconTalons(); });
        }
     
        /// <summary>
        /// Launches a falcon talon sesssion and gets contents of the 
        /// window consumed.
        /// </summary>
        public void InvokeFalconTalons()
        {
           // Build talon session
           FalconStore.MainSession = new TalonSession(
                FalconStore.FalconLogger,
                FalconStore.MainWindow,
                FalconSettings.OutputHeadDir,
                FalconSettings.FalconCliArgs.DebugConfiguration?.SuperTalonLogging
            );

            // Start the talon launcher here.
            FalconStore.MainSession.StartTalonSession(FalconStore.FalconSessionSetup.ScanAppPath);
            FalconStore.FalconLogger.WriteLogEntry("LAUNCHED TALON SESSION CORRECTLY!", LogEntryType.LOG_INFO);

            // Wait for updates to complete.
            if (FalconStore.UpdatesComplete) { FalconStore.FalconLogger.WriteLogEntry("UPDATES COMPLETE! STARTING TALON RPA TASKS", LogEntryType.LOG_INFO); }
            else
            {
                // Logging Info
                FalconStore.FalconLogger.WriteLogEntry("WAITING FOR UPDATES TO FINISH BEFORE LAUNCHING RPA TASKS...", LogEntryType.LOG_INFO);
                while (!FalconStore.UpdatesComplete) { }
                FalconStore.FalconLogger.WriteLogEntry("UPDATES COMPLETE! STARTING TALON RPA TASKS", LogEntryType.LOG_INFO);
            }

            // Build Elements and write them to file.
            FalconStore.MainSession.WriteTalonsAsJson();
            FalconStore.FalconLogger.WriteLogEntry("TALON CONTENTES HAVE BEEN PULLED AND STORED TO A JSON FILE!", LogEntryType.LOG_INFO);
        }

        // ---------------------------------- WINDOW EVENT CALLS FOR XAML BINDINGS ----------------------------------


        /// <summary>
        /// On Closing, kill the scan app as well.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FalconWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Kill the scan app here.
            if (FalconStore.MainSession?.AppProcess == null) { return; }
            FalconStore.MainSession.AppProcess.Kill();
        }
    }
}
