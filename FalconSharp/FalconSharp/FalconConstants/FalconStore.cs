﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERSConstructor;
using ERSConstructor.DynamicERS;
using FalconObjects.FalconCommandLineArgs;
using FalconObjects.FalconParameterObjects;
using FalconObjects.FalconScanSetup;
using FalconObjects.FalconVehicles;
using FalconSharpUpdater;
using IronPython.Compiler.Ast;
using Microsoft.Scripting.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using FalconPyEngine;
using FalconSharp.FalconResources;
using System.Threading;
using System.Windows.Interop;
using static FalconSharp.FalconConstants.FalconSettings;
using FalconTalons;

namespace FalconSharp.FalconConstants
{
    /// <summary>
    /// Application constants for all classes/models to access
    /// </summary>
    public static class FalconStore
    {
        // Current Session Id
        public static Guid SessionId;
        public static FalconSharpMainWindow MainWindow;
        public static TalonSession MainSession;

        // Logger Setup
        public static LoggingHelpers LoggerSetup;                   // Main logging instance for Falcon.
        public static DynamicERSLogger FalconLogger;                // Main Logger For Falcon.
        public static FalconSessionArgs FalconSessionSetup;         // Args objects for this session

        // IronPython Objects
        public static IronPyEngine IronEngine;                       // Iron Python engine.
        public static FalconUpdaterMain FalconUpdater;               // Updater for falcon

        // Check status of Python downloads.
        private static CancellationTokenSource TokenSource;
        public static bool UpdatesComplete => TokenSource.Token.IsCancellationRequested;

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Configure application constants here.
        /// </summary>
        public static bool SetupConstants()
        {
            // Run Falcon Startup
            Console.SetWindowSize(120, 35);
            SetupLogging();          // Builds new logging objects and starts a main logger
            if (!ParseArguments()) { return false; }  // Parse CLI Arguments to setup values on the app.
            GetUpdates();            // Pull down python updates and any other DLL based updating.
            SetupPyEngine();         // Configures a new ironpython engine.

            // Make JSON Output path and write settings for it.
            if (DebugConfiguration?.BuildJsonTemplates)
            {
                FalconLogger.WriteLogEntry("DEBUGGER IS ON! THIS WILL GENERATE NEW JSON FROM WHATEVER IS CURRENTLY IN THE C# CODE!", LogEntryType.LOG_WARN);
                string JsonOutput = Path.Combine(OutputHeadDir, "FalconScanJson");
                Directory.CreateDirectory(JsonOutput);
                FalconSessionWriter.WriteSettings(JsonOutput);  // Writes Settings models out to JSON
            }

            // Log Setup Passed
            FalconLogger.WriteLogEntry("FALCON HAS BEEN LAUNCHED WITHOUT ANY ERRORS! READY FOR TASKING", LogEntryType.LOG_WARN);
            return true;
        }
        /// <summary>
        /// Reads in a scan type and converts it to an enum.
        /// Stores the current scan session objects.
        /// </summary>
        public static bool SetupScanType(VehicleScan ScanType = VehicleScan.BASE)
        {
            // Set the types
            if (ScanType == VehicleScan.BASE)
            {
                // Set new value
                string Make = VehicleConfiguration?.VehicleMake.ToUpper();
                if (Enum.GetNames(typeof(VehicleScan)).Any(VehicleEnum => VehicleEnum == Make))
                {
                    var EnumNameIndex = Enum.GetNames(typeof(VehicleScan)).FindIndex(Name => Name == Make);
                    var ValueObj = Enum.GetValues(typeof(VehicleScan)).GetValue(EnumNameIndex);
                    ScanType = (VehicleScan)ValueObj;
                    ScanType = ScanType.ScanApp();
                }
                else 
                {
                    // Can't scan this car
                    FalconLogger.WriteLogEntry($"ERROR! CAN NOT SCAN VEHICLES WITH MAKE {ScanType.ToString().ToUpper()}! FAILING OUT", LogEntryType.LOG_FATAL);
                    return false;
                }
            }

            // Set path of config file.
            string PathValue = Path.Combine(OutputHeadDir, "FalconScanJson", $"FalconScan{ScanType.ToString().ToUpper()}.json");

            // Log info
            FalconLogger.WriteLogEntry($"EXPECTING FILE FOR SCAN TYPE: {ScanType}", LogEntryType.LOG_DEBUG);
            FalconLogger.WriteLogEntry($"CHECKING FOR NEW SCAN SESSION CONFIG FILE AT: {PathValue}", LogEntryType.LOG_DEBUG);

            // Read, convert, cast, store.
            if (File.Exists(PathValue))
            {
                // Store value here.
                string Content = File.ReadAllText(PathValue);
                var ConvertedJson = JsonConvert.DeserializeObject<FalconSessionArgs>(Content);

                // Store main config and the session specific config here.
                FalconSessionSetup = ConvertedJson;

                // Log output content and status.
                string UnformattedJson = JsonConvert.SerializeObject(FalconSessionSetup, Formatting.None);
                FalconLogger.WriteLogEntry($"FILE FOR SCAN TYPE {ScanType} WAS FOUND OK!", LogEntryType.LOG_DEBUG);
                FalconLogger.WriteLogEntry($"FILE CONTENTS: {UnformattedJson}", LogEntryType.LOG_TRACE);

                // Log passed
                FalconLogger.WriteLogEntry($"FALCON HAS CONFIGURED A NEW {ScanType} SESSION TYPE CORRECTLY!", LogEntryType.LOG_WARN);
                return true;
            }

            // Failed. Log a failure output
            FalconLogger.WriteLogEntry(new InvalidOperationException($"INVALID SCAN TYPE IS PASSED IN! TYPE FOUND: {ScanType}"), LogEntryType.LOG_FATAL);
            return false;
        }


        /// <summary>
        /// Configure logging
        /// </summary>
        private static void SetupLogging()
        {
            // Build Logging Invoker
            SessionId = Guid.NewGuid();
            LoggerSetup = new LoggingHelpers(
                LoggerSettings.CustomLoggingName,
                LoggerSettings.BaseOutput,
                LoggerSettings.MinLogging,
                LoggerSettings.ConnectionString
            );

            // Set Head Dir
            OutputHeadDir = LoggerSettings.BaseOutput;
            LoggingHelpers.SetLoggingContext(SessionId, "", "");

            // Write logging setup and settings setup went ok.
            FalconLogger = LoggerSetup.InstanceMainLogger;
            FalconLogger.WriteLogEntry("SETUP FALCON LOGGER CONSTANTS CORRECTLY!", LogEntryType.LOG_WARN);
            FalconLogger.WriteLogEntry($"LOGGER NAME: {LoggerSettings.CustomLoggingName}", LogEntryType.LOG_DEBUG);
            FalconLogger.WriteLogEntry($"LOGGER OUTPUT: {LoggerSettings.BaseOutput}", LogEntryType.LOG_DEBUG);
            FalconLogger.WriteLogEntry($"LOGGER SESSION ID: {SessionId.ToString("D".ToUpper())}", LogEntryType.LOG_DEBUG);
            FalconLogger.WriteLogEntry($"LOGGER CONNECTION STRING: {LoggerSettings.ConnectionString}", LogEntryType.LOG_TRACE);
            FalconLogger.WriteLogEntry("SETTINGS WERE READ IN OK AND LOGGING HAS BEEN STARTED", LogEntryType.LOG_INFO);
        }
        /// <summary>
        /// Parses in the command line arguments from this application
        /// </summary>
        private static bool ParseArguments()
        {
            // Log status
            FalconLogger.WriteLogEntry($"SETTING UP FALCON ARGUMENTS NOW...", LogEntryType.LOG_WARN);

            // Get Args and store them.
            var Args = Environment.GetCommandLineArgs().Skip(1).ToArray();
            FalconLogger.WriteLogEntry($"ARGUMENTS PASSED: {string.Join(" ", Args)}", LogEntryType.LOG_TRACE);
            if (Args.Length < 4)
            {
                // Can't be done.
                FalconLogger.WriteLogEntry($"ERROR! ARGS PROVIDED DID NOT PASS REQUIREMENTS! USING FALLBACK", LogEntryType.LOG_ERROR);
                if (!Debugger.IsAttached && !FalconCliArgs.ForceDebuggingSession) return false;
                Args = Properties.Resources.DebugArgsFallback.Split(' ');
                FalconLogger.WriteLogEntry($"NEW FALLBACK ARGS PASSED: {string.Join(" ", Args)}", LogEntryType.LOG_TRACE);
                FalconLogger.WriteLogEntry($"REPARSING NEW ARGUMENT VALUES NOW...", LogEntryType.LOG_TRACE);
            }
            
            // Build FalconArgs now
            FalconBaseArguments = new FalconArguments(Args);
            if (FalconBaseArguments.FalconArgsObject == null || !FalconBaseArguments.ParsePassed)
            {
                // Failed. Return
                FalconLogger.WriteLogEntry("FAILED TO SETUP FALCON ARGUMENTS!", LogEntryType.LOG_FATAL);
                FalconLogger.WriteLogEntry($"ARGS PROVIDED: {string.Join(" ", Args)}", LogEntryType.LOG_FATAL);

                // Set Debug Args if possible and reparse
                if (!Debugger.IsAttached && !FalconCliArgs.ForceDebuggingSession) { return false; }
                Args = Properties.Resources.DebugArgsFallback.Split(' ');
                FalconLogger.WriteLogEntry($"FORCING CLI DEBUGGING BASED ON PARAMS FOR ARGUMENTS", LogEntryType.LOG_ERROR);
                FalconLogger.WriteLogEntry($"NEW FALLBACK ARGS PASSED: {string.Join(" ", Args)}", LogEntryType.LOG_TRACE);
                FalconLogger.WriteLogEntry($"REPARSING NEW ARGUMENT VALUES NOW...", LogEntryType.LOG_TRACE);
                FalconBaseArguments = new FalconArguments(Args);

                // Check Status
                if (!FalconBaseArguments.ParsePassed) 
                {
                    FalconLogger.WriteLogEntry($"ERROR! CAN'T PARSE FALLBACK VALUES! THIS IS CRITICAL", LogEntryType.LOG_FATAL);
                    return false; 
                }
            }

            // Log passed and set context.
            var VehicleInfo = FalconCliArgs.VehicleConfiguration;
            FalconLogger.WriteLogEntry("SETUP FALCON ARGUMENTS CORRECTLY!", LogEntryType.LOG_WARN);
            FalconLogger.WriteLogEntry("VEHICLE CONFIGURATION AND DEBUGGING ARGUMENTS STORED", LogEntryType.LOG_INFO);
            FalconLogger.WriteLogEntry($"VEHICLE YEAR {VehicleInfo.VehicleYear}", LogEntryType.LOG_DEBUG);
            FalconLogger.WriteLogEntry($"VEHICLE MAKE {VehicleInfo.VehicleMake}", LogEntryType.LOG_DEBUG);
            FalconLogger.WriteLogEntry($"VEHICLE MODEL {VehicleInfo.VehicleModel}", LogEntryType.LOG_DEBUG);
            FalconLogger.WriteLogEntry($"VEHICLE VIN {VehicleInfo.VehicleVin}", LogEntryType.LOG_DEBUG);

            // Set Context
            FalconLogger.WriteLogEntry("SETTING LOGGER CONTEXT VALUES NOW...", LogEntryType.LOG_INFO);
            FalconLogger.WriteLogEntry($"CONTEXT SESSION TO SET: {SessionId.ToString("D".ToUpper())}", LogEntryType.LOG_DEBUG);
            FalconLogger.WriteLogEntry($"CONTEXT YMM STRING TO SET: {VehicleInfo.YMMString}", LogEntryType.LOG_DEBUG);
            FalconLogger.WriteLogEntry($"CONTEXT VIN TO SET: {VehicleInfo.VehicleVin}", LogEntryType.LOG_DEBUG);
            LoggingHelpers.SetLoggingContext(SessionId, VehicleInfo.YMMString, VehicleInfo.VehicleVin);

            // Set JSON Defaults.
            bool SuperLogOn = (bool)FalconCliArgs.DebugConfiguration?.SuperTalonLogging;
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                // Set Logging State based on the logging type.
                Formatting = SuperLogOn ? Formatting.Indented : Formatting.None,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            };

            // Return Passed.
            return true;
        }
        /// <summary>
        /// Update falcon support
        /// </summary>
        private static void GetUpdates()
        {
            // Log Step then Build and invoke Updates
            FalconLogger.WriteLogEntry("BOOTING FALCON UPDATER AND CHECKING FOR UPDATES NOW", LogEntryType.LOG_WARN);
            bool SuperLogOn = (bool)FalconCliArgs.DebugConfiguration?.SuperTalonLogging;
            FalconUpdater = new FalconUpdaterMain(FalconLogger, AwsConfiguration, SuperLogOn);

            // Setup Token Source here.
            TokenSource = new CancellationTokenSource();
            FalconLogger.WriteLogEntry("TASK TOKEN SOURCE CREATED FOR BG UPDATING OK!", LogEntryType.LOG_DEBUG);

            // BG Update it.
            Task.Run(() =>
            {
                // Log BG
                FalconLogger.WriteLogEntry("UPDATING IRON PYTHON IN THE BACKGROUND...", LogEntryType.LOG_WARN);

                // Run Updating process.
                FalconUpdater.UpdateIronPyStd();
                FalconUpdater.UpdateIronPyScripts();

                // Cancel Token here.
                TokenSource.Cancel();

            }, TokenSource.Token);
        }
        /// <summary>
        /// Launch our python engine.
        /// </summary>
        private static void SetupPyEngine()
        {
            // Log Setup for Python Engine.
            FalconLogger.WriteLogEntry("SPINNING UP A NEW PYTHON ENGINE FOR FALCON NOW...", LogEntryType.LOG_WARN);
            bool SuperLogOn = (bool)FalconCliArgs.DebugConfiguration?.SuperTalonLogging;

            // Setup IronPython Engine
            IronEngine = new IronPyEngine(FalconLogger, SuperLogOn);
            string BasePath = FalconUpdater.FileLocations.IronPythonLocalBase;
            IronEngine.AddSearchPaths($"{BasePath}\\IronPythonStd");
            IronEngine.AddSearchPaths($"{BasePath}\\IronPythonScripts");
        }
    }
}
