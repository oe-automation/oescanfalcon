﻿using FalconSharp.FalconMVVM.ViewModels;

namespace FalconSharp.FalconMVVM.Models
{
    public static class FalconViewStore
    {
        // Main Window Object
        public static FalconSharpMainWindow Main { get; private set; }

        // View Model Objects
        // public static TitleAndVersionViewModel TitleAndVersionViewModel { get => Main?.TitleAndVersionView?.ViewModel; set => Main.TitleAndVersionView.ViewModel = value; }
     
        // ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes the Constants objects. Contains view values
        /// </summary>
        /// <param name="MainView">MainWindow Object view</param>
        public static void InitConstants(FalconSharpMainWindow MainView)
        {
            // Setup Main Values
            Main = MainView;
        }
    }
}
