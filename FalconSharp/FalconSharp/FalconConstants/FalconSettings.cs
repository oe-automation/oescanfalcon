﻿using ERSConstructor.DynamicERS;
using FalconObjects.FalconCommandLineArgs;
using FalconObjects.FalconParameterObjects;
using FalconObjects.FalconScanSetup;
using FalconSharp.FalconStyles.StyleConfiguration;
using FalconSharp.FalconStyles.StyleConfiguration.StyleModels;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FalconSharp.FalconConstants
{
    /// <summary>
    /// Global Settings Values for use on all parts of this app
    /// </summary>
    public static class FalconSettings
    {
        // Settings and Themes Files
        public static string OutputHeadDir;                        
        public static string AppSettingsFile = @"FalconResources/FalconSettings.json";

        // CLI ARGUMENT OBJECT! THIS CONTAINS A TON OF INFO
        internal static FalconArguments FalconBaseArguments;
        public static dynamic FalconCliArgs => FalconBaseArguments.FalconArgsObject;
        public static dynamic DebugConfiguration => FalconCliArgs?.DebugConfiguration;
        public static dynamic VehicleConfiguration => FalconCliArgs?.VehicleConfiguration;

        // Main Static JSON Settings
        public static FalconAWSConfiguration AwsConfiguration;      // Main Settings object for Falcon.
        public static FalconLoggingConfiguration LoggerSettings;    // Main Settings object for Falcon.

        // Color and Setting Configuration Objects from the config helpers
        public static AppThemeConfiguration ThemeConfiguration;
        public static AppTheme CurrentAppTheme => ThemeConfiguration.CurrentAppTheme;

        // ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Initializes the app settings class. Contains themes and config settings
        /// </summary>
        public static void ParseSettings()
        {
            // Setup Logger and Settings First.
            JObject ConfigFileContent = JObject.Parse(File.ReadAllText(AppSettingsFile));
            AwsConfiguration = ConfigFileContent["AwsConfiguration"].ToObject<FalconAWSConfiguration>();
            LoggerSettings = ConfigFileContent["LoggingConfiguration"].ToObject<FalconLoggingConfiguration>();
        }
    }
}
