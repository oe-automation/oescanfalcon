﻿using System;
using System.IO;
using System.Reflection;
using ERSConstructor;
using ERSConstructor.DynamicERS;
using FalconObjects.FalconScanSetup;
using FalconObjects.FalconVehicles;
using Newtonsoft.Json;

namespace FalconSharp.FalconResources
{
    public static class FalconSessionWriter
    {
        // Scan Config Objects
        private static FalconSessionArgs FalconScanBase;     // Base Scan Config
        private static FalconSessionArgs FalconScanC3P;      // C3P configuration Object
        private static FalconSessionArgs FalconScanFJDS;     // FJDS configuration Object
        private static FalconSessionArgs FalconScanFDRS;     // FDRS configuration Object
        private static FalconSessionArgs FalconScanGDS2;     // GDS2 configuration Object
        private static FalconSessionArgs FalconScanHDS;      // HDS configuration Object
        private static FalconSessionArgs FalconScanIHDS;     // IHDS configuration Object
        private static FalconSessionArgs FalconScanTIS;      // TIS configuration Object

        // ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Writes the settings object out for the types of scans.
        /// </summary>
        public static void WriteSettings(string BaseOutput)
        {
            // Logger init
            var FalconLogger = LoggingHelpers.MainLogger;
            FalconLogger.WriteLogEntry("SETTING UP NEW SETTINGS OBJECTS FOR SCAN ENTRIES NOW...", LogEntryType.LOG_INFO);
            FalconLogger.WriteLogEntry($"JSON OUTPUT LOCATION: {BaseOutput}", LogEntryType.LOG_DEBUG);

            // Get Fields.
            int FieldCounter = 0;
            var PrivateStatics = typeof(FalconSessionWriter).GetFields(BindingFlags.Static | BindingFlags.NonPublic);
            FalconLogger.WriteLogEntry($"GOT A TOTAL OF {PrivateStatics.Length} FIELDS TO POPULATE", LogEntryType.LOG_DEBUG);
            foreach (var FieldValue in PrivateStatics)
            {
                // Loop them all and write out a config object for each
                string FieldType = FieldValue.Name.Replace("FalconScan", string.Empty).ToUpper();
                var ScanType = (VehicleScan)Enum.Parse(typeof(VehicleScan), FieldType);
                FalconLogger.WriteLogEntry($"FIELD: [{FieldCounter}]-- {FieldValue.Name} IS BEING CONFIGURED NOW", LogEntryType.LOG_DEBUG);
                var ValueObj = (FalconSessionArgs)FieldValue.GetValue(null) ?? GetConfiguration(ScanType);

                // File Output Information
                string BaseJString = JsonConvert.SerializeObject(ValueObj, Formatting.Indented);
                string BaseJString_NoTabs = JsonConvert.SerializeObject(ValueObj, Formatting.None);
                string OutputFile = Path.Combine(BaseOutput, $"{FieldValue.Name}.json");

                // Check if FIle Exists
                if (!File.Exists(OutputFile))
                {
                    // Write and log content output
                    File.WriteAllText(OutputFile, BaseJString);
                    FalconLogger.WriteLogEntry($"WROTE NEW SCAN ARGS CONFIG OUTPUT OK!", LogEntryType.LOG_DEBUG);
                    FalconLogger.WriteLogEntry($"CONFIG FILE: {OutputFile}", LogEntryType.LOG_DEBUG);
                    FalconLogger.WriteLogEntry($"FILE CONTENT: {BaseJString_NoTabs}", LogEntryType.LOG_TRACE);
                }
                else
                {
                    // If the file is the same as the new object, then move on.
                    if (File.ReadAllText(OutputFile) == BaseJString) FalconLogger.WriteLogEntry($"FIELD: [{FieldCounter}]-- {FieldValue.Name} CURRENT JSON CONFIG IS CURRENT!", LogEntryType.LOG_INFO);
                    else
                    {
                        // Write and log content output
                        File.WriteAllText(OutputFile, BaseJString);
                        FalconLogger.WriteLogEntry($"WROTE NEW SCAN ARGS CONFIG OUTPUT OK!", LogEntryType.LOG_DEBUG);
                        FalconLogger.WriteLogEntry($"CONFIG FILE: {OutputFile}", LogEntryType.LOG_DEBUG);
                        FalconLogger.WriteLogEntry($"FILE CONTENT: {BaseJString_NoTabs}", LogEntryType.LOG_TRACE);
                    }
                }

                // Increase counter
                FieldCounter += 1;
            }

            // Write Status
            FalconLogger.WriteLogEntry("WROTE OUT BASE CONFIG AND ALL SUPPORTED SCAN CONFIG TYPES OK!", LogEntryType.LOG_INFO);
        }

        /// <summary>
        /// Gets a falcon scan config based on the type given in
        /// </summary>
        /// <returns>A Falcon scan config for the current scan session type.</returns>
        public static FalconSessionArgs GetConfiguration(VehicleScan TypeOfConfig)
        {
            // Switch the type for the input value. Return based on that
            switch (TypeOfConfig)
            {
                // BASE CONFIG
                case VehicleScan.BASE:
                case VehicleScan.INVALID:
                    #region BASE CONFIGURATION OBJECT
                    var FalconBase = new FalconSessionArgs();
                    FalconBase.ScanType = VehicleScan.BASE;
                    FalconBase.WorkingDirectory = "C:\\OPUS-IVS\\Falcon";
                    FalconBase.StartupScanTasks = new FalconScanStep[]
                    {
                        new FalconScanStep() { IndexOfStep = 0, StepLength = 100, StepName = "Launch OE App", StepShowsProgress = true },
                        new FalconScanStep() { IndexOfStep = 1, StepLength = 100, StepName = "Setup Environment", StepShowsProgress = false },
                        new FalconScanStep() { IndexOfStep = 2, StepLength = 100, StepName = "Check For License", StepShowsProgress = true },
                        new FalconScanStep() { IndexOfStep = 3, StepLength = 100, StepName = "Select VCI", StepShowsProgress = true },
                    };
                    FalconBase.CompletedScanTasks = new FalconScanStep[]
                    {
                        new FalconScanStep() { IndexOfStep = 0, StepLength = 100, StepName = "Close OE Scan App", StepShowsProgress = true },
                        new FalconScanStep() { IndexOfStep = 1, StepLength = 100, StepName = "Upload OE Scan Report", StepShowsProgress = true },
                        new FalconScanStep() { IndexOfStep = 2, StepLength = 100, StepName = "Exit OE Scan", StepShowsProgress = true },
                    };

                    // Return base configuration
                    FalconScanBase = FalconBase;
                    return FalconBase;
                #endregion

                // OE SCAN APP CONFIGS
                case VehicleScan.TIS:
                    #region TIS Configuration
                    var FalconTIS = new FalconSessionArgs();
                    FalconTIS.ScanType = VehicleScan.TIS;
                    FalconTIS.WorkingDirectory = "C:\\OPUS-IVS\\Falcon";
                    FalconTIS.ScanAppPath = "C:\\Program Files (x86)\\Toyota Diagnostics\\Techstream\\bin\\MainMenu.exe";

                    // Set Scan Steps
                    var BaseObj = GetConfiguration(VehicleScan.BASE);
                    FalconTIS.StartupScanTasks = BaseObj.StartupScanTasks;
                    FalconTIS.CompletedScanTasks = BaseObj.CompletedScanTasks;
                    FalconTIS.ScanSteps = new FalconScanStep[]
                    {
                        new FalconScanStep() { IndexOfStep = 0, StepLength = 100, StepName = "Connect To Vehicle", StepShowsProgress = true },
                        new FalconScanStep() { IndexOfStep = 1, StepLength = 100, StepName = "Configure Options", StepShowsProgress = true },
                        new FalconScanStep() { IndexOfStep = 2, StepLength = 100, StepName = "Perform Health Check", StepShowsProgress = true },
                    };

                    // Return here
                    FalconScanTIS = FalconTIS;
                    return FalconTIS;
                #endregion
                case VehicleScan.C3P:
                    return GetConfiguration(VehicleScan.BASE);
                case VehicleScan.FJDS:
                    return GetConfiguration(VehicleScan.BASE);
                case VehicleScan.FDRS:
                    return GetConfiguration(VehicleScan.BASE);
                case VehicleScan.GDS2:
                    return GetConfiguration(VehicleScan.BASE);
                case VehicleScan.HDS:
                    return GetConfiguration(VehicleScan.BASE);
                case VehicleScan.IHDS:
                    return GetConfiguration(VehicleScan.BASE);

                // Default return base
                default: return GetConfiguration(VehicleScan.BASE);
            }
        }
    }
}
