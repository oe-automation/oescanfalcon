﻿using ControlzEx.Theming;
using FalconSharp.FalconConstants;
using FalconSharp.FalconMVVM.Models;
using FalconSharp.FalconStyles.StyleConfiguration;
using FalconSharp.FalconStyles.StyleConfiguration.AppStyleLogic;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace FalconSharp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Runs this on startup to configure themes and other settings
        /// </summary>
        /// <param name="e">Event args</param>
        protected override void OnStartup(StartupEventArgs e)
        {
            // Startup override.
            base.OnStartup(e);

            // Configure Settings and Constants
            FalconSettings.ParseSettings();
            FalconSettings.ThemeConfiguration = new AppThemeConfiguration();
            FalconSettings.ThemeConfiguration.GenerateThemeSettings();

            // Test color setting.
            ThemeManager.Current.SyncTheme();
            AppColorSetter.SetAppColorScheme(FalconSettings.CurrentAppTheme);
        }
    }
}
