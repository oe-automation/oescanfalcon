﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FalconSharp.FalconMVVM.ViewModels;

namespace FalconSharp.FalconMVVM.Views
{
    /// <summary>
    /// Interaction logic for TitleAndVersionView.xaml
    /// </summary>
    public partial class TitleAndVersionView : UserControl
    {
        // The ViewModel for this view control
        public TitleAndVersionViewModel ViewModel { get; set; }

        /// <summary>
        /// Builds a new title and version control
        /// </summary>
        public TitleAndVersionView()
        {
            InitializeComponent();
            this.ViewModel = new TitleAndVersionViewModel();
        }

        // ---------------------------------------------------------------------------------------------

        /// <summary>
        /// On loaded, we want to setup our new viewmodel object and populate values
        /// </summary>
        /// <param name="sender">Sending object</param>
        /// <param name="e">Events attached to it.</param>
        private void TitleAndVersionView_OnLoaded(object sender, RoutedEventArgs e)
        {
            // Setup a new ViewModel
            this.ViewModel.SetupViewControl(this);
            this.DataContext = this.ViewModel;
        }
    }
}
