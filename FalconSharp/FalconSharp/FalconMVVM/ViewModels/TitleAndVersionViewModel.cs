﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using FalconSharp.FalconMVVM.ViewModels;
using FalconSharp.FalconMVVM.Views;

namespace FalconSharp.FalconMVVM.ViewModels
{
    /// <summary>
    /// VM Logic for our title and version content display
    /// </summary>
    public class TitleAndVersionViewModel : ViewModelControlBase
    {
        #region Private Values
        
        private TitleAndVersionView View => (TitleAndVersionView)BaseViewControl;

        private string _applicationTitle;
        private string _applicationVersion;

        #endregion

        // Public objects for use
        public string ApplicationTitle
        {
            get => _applicationTitle;
            set => PropertyUpdated(value);
        }       // Title of the application
        public string ApplicationVersion
        {
            get => _applicationVersion;
            set => PropertyUpdated(value);
        }     // Version of the application

        // ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Configures a new View Object to work with.
        /// </summary>
        /// <param name="ContentView">The View to store</param>
        public override void SetupViewControl(UserControl ContentView)
        {
            // Set the view contents 
            this.BaseViewControl = ContentView;

            // Set the title and version
            this.ApplicationTitle = "FalconSharp - Talon Sniffer";
            // this.ApplicationVersion = AppSettings.AppConfigHelper.ConfigurationSettings.ApplicationSettings.ApplicationVersion;
        }
    }
}
