﻿using FalconSharp.FalconMVVM.Models;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Controls;

namespace FalconSharp.FalconMVVM.ViewModels
{
    /// <summary>
    /// Base class for Model objects on the UI
    /// </summary>
    public class ViewModelControlBase
    {
        // View object to setup and custom setter
        internal UserControl BaseViewControl;
        public virtual void SetupViewControl(UserControl ContentView) { BaseViewControl = ContentView; }

        // ---------------------------------------------------------------------------------------------

        #region Property Changed Event Setup

        // Property Changed event.
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(PropertyName));
        }

        #endregion

        /// <summary>
        /// Updates the property on this view model and sets a prop notify event
        /// </summary>
        /// <typeparam name="TPropertyType">Type of property</typeparam>
        /// <param name="PropertyName">Name of property </param>
        /// <param name="Value">Value being used</param>
        internal void PropertyUpdated<TPropertyType>(TPropertyType Value, [CallerMemberName] string PropertyName = null)
        {
            // Update Globals and the current value
            UpdatePrivatePropertyValue(this, PropertyName, Value);
            UpdateViewModelPropertyValue(this);

            // Run prop changed event and set private value
            OnPropertyChanged(PropertyName);
        }

        /// <summary>
        /// Property Changed without model binding
        /// </summary>
        /// <param name="PropertyName">Name of property to emit change for</param>
        /// <param name="NotifierObject">Object sending this out</param>
        private void UpdatePrivatePropertyValue<TPropType>(object NotifierObject, string PropertyName, TPropType NewPropValue)
        {
            // Store the type of the sender
            var InputObjType = NotifierObject.GetType();

            // Loop all fields, find the private value and store it
            var MembersFound = InputObjType.GetMembers(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            var MemberObject = MembersFound.FirstOrDefault(FieldObj =>
                FieldObj.Name.Contains("_") &&
                FieldObj.Name.Substring(1).ToUpper() == PropertyName.ToUpper());

            // Set the model property value here and raise an args value.
            switch (MemberObject.MemberType)
            {
                // Sets the value on the class into the current invoking object
                case MemberTypes.Field:
                    FieldInfo InvokerField = (FieldInfo)MemberObject;
                    InvokerField.SetValue(NotifierObject, NewPropValue);
                    break;

                case MemberTypes.Property:
                    PropertyInfo InvokerProperty = (PropertyInfo)MemberObject;
                    InvokerProperty.SetValue(NotifierObject, NewPropValue);
                    break;

                default:
                    throw new NotImplementedException($"THE INVOKED MEMBER {PropertyName} COULD NOT BE FOUND!");
            }
        }

        /// <summary>
        /// Updates the globals with the new values configured into this object 
        /// </summary>
        /// <param name="ViewModelObject">Object to update</param>
        private void UpdateViewModelPropertyValue(ViewModelControlBase ViewModelObject)
        {
            // Get the types on the globals first.
            var AppViewStoreType = typeof(FalconViewStore);
            var ViewModelTypeName = Type.GetType(ViewModelObject.ToString());
            if (AppViewStoreType == null) { throw new NullReferenceException($"THE TYPE {typeof(FalconViewStore).ToString()} COULD NOT BE FOUND!"); }

            // Gets all the members and sets one to update
            var AppStoreMembers = AppViewStoreType.GetMembers();
            var MemberToUpdate = AppStoreMembers.FirstOrDefault(MemberObj => MemberObj.Name == ViewModelTypeName.Name);
            if (MemberToUpdate == null) { throw new NullReferenceException($"THE MEMBER {nameof(ViewModelTypeName.Name)} COULD NOT BE FOUND!"); }

            // Set the value here
            switch (MemberToUpdate.MemberType)
            {
                // Sets the value on the class into the current invoking object
                case MemberTypes.Field:
                    FieldInfo MemberAsField = (FieldInfo)MemberToUpdate;
                    MemberAsField.SetValue(null, ViewModelObject);
                    break;
                case MemberTypes.Property:
                    PropertyInfo MemberAsProperty = (PropertyInfo)MemberToUpdate;
                    MemberAsProperty.SetValue(null, ViewModelObject);
                    break;

                // If neither field or property fail out
                default:
                    throw new NotImplementedException($"THE REQUESTED MEMBER {nameof(ViewModelObject)} COULD NOT BE FOUND!");
            }
        }
    }
}
