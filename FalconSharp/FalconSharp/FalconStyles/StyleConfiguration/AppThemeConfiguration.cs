﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using ControlzEx.Theming;
using FalconSharp.FalconConstants;
using FalconSharp.FalconStyles.StyleConfiguration.AppStyleLogic;
using FalconSharp.FalconStyles.StyleConfiguration.DataConverters;
using FalconSharp.FalconStyles.StyleConfiguration.StyleModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FalconSharp.FalconStyles.StyleConfiguration
{
    public class AppThemeConfiguration
    {
        // Theme Objects
        public List<AppTheme> PresetThemes;         // Themes that exist regardless of the current operations
        public AppTheme CurrentAppTheme;            // The Current theme in use

        // ---------------------------------------------------------------------------------------------       

        /// <summary>
        /// Builds the App Settings config
        /// </summary>
        public void GenerateThemeSettings()
        {
            // Build Settings
            JObject ConfigFileContent = JObject.Parse(File.ReadAllText(FalconSettings.AppSettingsFile));
            var ThemeSettings = ConfigFileContent["FalconThemes"];

            // Store themes
            this.SetupPresetThemes();
            string Base = ThemeSettings["DesiredThemeBase"].ToObject<string>();
            string Secondary = ThemeSettings["DesiredThemeSecondary"].ToObject<string>();

            // Set if no theme wanted
            if (!string.IsNullOrEmpty(Base) & !string.IsNullOrEmpty(Secondary))
                CurrentAppTheme = new AppTheme("FalconSharp", Base, Secondary, ThemeType.DARK_COLORS);
            else
            {
                // Set Random Theme
                var RngInt = new Random();
                CurrentAppTheme = this.PresetThemes[RngInt.Next(0, this.PresetThemes.Count - 1)];
            }

            // Set Theme here
            ThemeSettings["CurrentTheme"] = JToken.FromObject(CurrentAppTheme);
            ConfigFileContent["FalconThemes"] = ThemeSettings;
            File.WriteAllText(FalconSettings.AppSettingsFile, JsonConvert.SerializeObject(ConfigFileContent, Formatting.Indented));
        }


        /// <summary>
        /// Adds a new theme object to this settings model
        /// </summary>
        /// <param name="ThemeName">Name of theme</param>
        /// <param name="BaseColor">Main color</param>
        /// <param name="ShowcaseColor">Background Color</param>
        public AppTheme GenerateNewTheme(string ThemeName, string BaseColor, string ShowcaseColor, ThemeType TypeOfTheme)
        {
            // Get Color Uints
            int BaseUint = int.Parse(BaseColor.Replace("0x", string.Empty), NumberStyles.HexNumber);
            int ShowcaseUint = int.Parse(ShowcaseColor.Replace("0x", string.Empty), NumberStyles.HexNumber);

            // Convert the Uints into colors.
            return GenerateNewTheme(ThemeName, System.Drawing.Color.FromArgb(BaseUint), System.Drawing.Color.FromArgb(ShowcaseUint), TypeOfTheme);
        }
        /// <summary>
        /// Adds a new theme object to this settings model
        /// </summary>
        /// <param name="ThemeName">Name of theme</param>
        /// <param name="BaseColor">Main color</param>
        /// <param name="ShowcaseColor">Background Color</param>
        public AppTheme GenerateNewTheme(string ThemeName, System.Drawing.Color BaseColor, System.Drawing.Color ShowcaseColor, ThemeType TypeOfTheme)
        {
            // Build the new theme here
            switch (TypeOfTheme)
            {
                // Generate the custom schemes here.
                case ThemeType.DARK_COLORS:
                    string DarkBaseHexString = CustomColorConverter.HexConverter(BaseColor);
                    string DarkShowcaseHexString = CustomColorConverter.HexConverter(ShowcaseColor);
                    var DarkNewTheme = new AppTheme(ThemeName, DarkBaseHexString, DarkShowcaseHexString, TypeOfTheme);

                    // Return the new theme object
                    return DarkNewTheme;
                    

                case ThemeType.LIGHT_COLORS:
                    string LightBaseHexString = CustomColorConverter.HexConverter(BaseColor);
                    string LightShowcaseHexString = CustomColorConverter.HexConverter(ShowcaseColor);
                    var LightNewTheme = new AppTheme(ThemeName, LightBaseHexString, LightShowcaseHexString, TypeOfTheme);

                    // Return the new theme object
                    return LightNewTheme;
            }

            // Throw an ex if we get he1re.
            throw new InvalidOperationException("THEME COULD NOT BE ADDED SINCE COLOR PRESET WAS INVALID!");
        }


        /// <summary>
        /// Generates all the preset theme options we can use
        /// </summary>
        public void SetupPresetThemes()
        {
            // List of colors to setup as themes
            if (this.PresetThemes == null) { this.PresetThemes = new List<AppTheme>(); }
            var PresetThemes = new List<(string, string, string, ThemeType)>()
            {
                new("JCanaLog - Deep Purple", "0x54377A", "0x37557A", ThemeType.DARK_COLORS),
                new("JCanaLog - Sky Blue", "0x15B6CF", "0x14631B", ThemeType.DARK_COLORS),
                new("JCanaLog - Ocean Green", "0x1F6F91", "0x436107", ThemeType.DARK_COLORS),
            };

            // Add these themes into the whole setup now.
            foreach (var ThemeColor in PresetThemes)
            {
                // Build theme and add to list of current
                var NextTheme = GenerateNewTheme(
                    ThemeColor.Item1,
                    ThemeColor.Item2,
                    ThemeColor.Item3, 
                    ThemeColor.Item4
                );

                // Add to list.
                this.PresetThemes.Add(NextTheme);
                ThemeManager.Current.AddTheme(NextTheme.MahThemeObject);
            }
        }
    }
}
