﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using ControlzEx.Theming;
using FalconSharp.FalconStyles.StyleConfiguration.DataConverters;
using FalconSharp.FalconStyles.StyleConfiguration.StyleModels;
using JCanalogClient.MVVM.Styles;

namespace FalconSharp.FalconStyles.StyleConfiguration.AppStyleLogic
{
    /// <summary>
    /// Class used to help configure coloring setup for themes
    /// </summary>
    public static class AppColorSetter
    {        
        /// <summary>
        /// Sets the application theme based ona theme object.
        /// </summary>
        /// <param name="ThemeToSet">Object used for resources to the theme</param>
        public static void SetAppColorScheme(AppTheme ThemeToSet)
        {
            // Get Color Values and set.
            var Primary = ThemeToSet.PrimaryColor.ToMediaColor();
            var Secondary = ThemeToSet.PrimaryColor.ToMediaColor();

            // Set theme and update app metro and the app style sheets
            SetAppColorScheme(Primary, Secondary, ThemeToSet.TypeOfTheme);

            // Get a MahTheme and set it.
            var BuiltMahTheme = RuntimeThemeGenerator.Current.GenerateRuntimeTheme(
                (ThemeToSet.TypeOfTheme == ThemeType.DARK_COLORS ? "Dark" : "Light"),
                ThemeToSet.PrimaryColor.ToMediaColor()
            );
            ThemeManager.Current.ChangeTheme(Application.Current, BuiltMahTheme);
        }
        /// <summary>
        /// Sets the App color scheme based on a single input color.
        /// </summary>
        /// <param name="BaseColor">Color to set from</param>
        public static void SetAppColorScheme(Color PrimaryColor, Color SecondaryColor, ThemeType ColorThemeType = ThemeType.DARK_COLORS)
        {
            // Get the resource dictionary
            var CurrentMerged = Application.Current.Resources.MergedDictionaries;
            var ColorResources = CurrentMerged.FirstOrDefault(Dict =>
                Dict.Source.ToString().Contains("AppColorTheme")
            );

            // Set Primary and Secondary Colors
            ColorResources["PrimaryColor"] = new SolidColorBrush(CustomColorShader.GenerateShadeColor(PrimaryColor, Colors.Black, 0));
            ColorResources["SecondaryColor"] = new SolidColorBrush(CustomColorShader.GenerateShadeColor(SecondaryColor, Colors.Black, 0));

            // Set the text base color
            if (ColorThemeType == ThemeType.DARK_COLORS)
                ColorResources["TextColorBase"] = new SolidColorBrush(Colors.White);
            if (ColorThemeType == ThemeType.LIGHT_COLORS)
                ColorResources["TextColorBase"] = new SolidColorBrush(Colors.Black);

            // Setup All The Values
            foreach (var ColorKey in ColorResources.Keys)
            {
                // If we have something we can't change move on.
                if (!ColorKey.ToString().Contains("_")) { continue; }

                // Split into three parts. Lighter/darker, base or secondary, and pct.
                string[] SplitName = ColorKey.ToString().Split('_');
                bool IsDarker = SplitName[1].ToUpper() == "DARKER";
                float PctToChange = (float)((double)int.Parse(SplitName[2]) / 100.0);

                // Create our colors now.
                if (SplitName[0].Contains("PrimaryColor") && IsDarker)
                    ColorResources[ColorKey] = new SolidColorBrush(CustomColorShader.GenerateShadeColor(PrimaryColor, Colors.Black, PctToChange));
                if (SplitName[0].Contains("PrimaryColor") && !IsDarker)
                    ColorResources[ColorKey] = new SolidColorBrush(CustomColorShader.GenerateShadeColor(PrimaryColor, Colors.White, PctToChange));
                if (SplitName[0].Contains("SecondaryColor") && IsDarker)
                    ColorResources[ColorKey] = new SolidColorBrush(CustomColorShader.GenerateShadeColor(SecondaryColor, Colors.Black, PctToChange));
                if (SplitName[0].Contains("SecondaryColor") && !IsDarker)
                    ColorResources[ColorKey] = new SolidColorBrush(CustomColorShader.GenerateShadeColor(SecondaryColor, Colors.White, PctToChange));
            }

            // Set the resource back here.
            Application.Current.Resources["AppColorTheme"] = ColorResources;
        }
    }
}
