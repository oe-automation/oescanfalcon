﻿namespace FalconObjects.FalconScanSetup
{
    /// <summary>
    /// Scan execution step objects
    /// </summary>
    public class FalconScanStep
    {
        public int IndexOfStep { get; set; }
        public int StepLength { get; set; }
        public string StepName{ get; set; }
        public bool StepShowsProgress { get; set; }
    }
}