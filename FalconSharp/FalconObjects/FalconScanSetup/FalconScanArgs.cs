﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FalconObjects.FalconVehicles;

namespace FalconObjects.FalconScanSetup
{
    /// <summary>
    /// Model object for a scan session
    /// </summary>
    public class FalconSessionArgs
    {
        public VehicleScan ScanType { get; set; }
        public string WorkingDirectory { get; set; }
        public string ScanAppPath { get; set; }
        public FalconScanStep[] StartupScanTasks { get; set; }
        public FalconScanStep[] CompletedScanTasks { get; set; }
        public FalconScanStep[] ScanSteps { get; set; }
    }
}
