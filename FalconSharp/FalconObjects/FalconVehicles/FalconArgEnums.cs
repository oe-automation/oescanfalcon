﻿namespace FalconObjects.FalconVehicles
{
    /// <summary>
    /// Makes for vehicles
    /// </summary>
    public enum VehicleScan : uint
    {
        // LAYOUT
        // 0x[00][00][00]
        // Group 1: Scan App (01 CP3)
        // Group 2: Make Mask (01 NISSAN)
        // Group 3: Model Type (** MODEL)

        // CURRENT CAR

        // SCAN TYPE MASKS
        INVALID = 0xFFFFFFFF,
        BASE = 0x00000000,

        // OE Apps
        C3P = 0x100000,      // C3P
        FJDS = 0x200000,     // FJDS
        FDRS = 0x210000,     // FDRS
        GDS2 = 0x300000,     // GDS2
        HDS = 0x400000,      // HDS
        IHDS = 0x410000,     // IHDS
        TIS = 0x500000,      // TIS

        // --------------------------------------------------------------

        // C3P
        NISSAN = C3P | 0x001000,
        INFINITI = C3P | 0x002000,

        // FJDS/FDRS (APPLY SWITCH BASED ON YEAR/MODEL!)
        FORD = FJDS | 0x001000,
        LINCOLN = FJDS | 0x002000,
        MERCURY = FJDS | 0x003000,

        // GDS2
        BUICK = GDS2 | 0x001000,
        CHEVROLET = GDS2 | 0x002000,
        CADILLAC = GDS2 | 0x003000,
        DODGE = GDS2 | 0x004000,
        GMC = GDS2 | 0x005000,

        // IHDS/HDS (APPLY SWITCH BASED ON YEAR/MODEL!)
        HONDA = HDS | 0x001000,
        ACURA = HDS | 0x002000,

        // TIS
        TOYOTA = TIS | 0x001000,
        LEXUS = TIS | 0x002000,
        SCION = TIS | 0x003000,
    }
}
