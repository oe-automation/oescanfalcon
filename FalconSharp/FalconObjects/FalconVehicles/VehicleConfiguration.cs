﻿namespace FalconObjects.FalconVehicles
{
    public static class DetermineVehicleMake
    {
        #region Make Based Bools.
        // Checks if the vehicle attached is a Toyota Variant.
        private static bool IsToyotaMake(this string Make)
        {
            if (string.IsNullOrEmpty(Make)) { return false; }
            bool IsToyota =
                (Make.ToUpper() == "TOYOTA") ||
                (Make.ToUpper() == "LEXUS") ||
                (Make.ToUpper() == "SCION");
            return IsToyota;
        }

        // Checks if the vehicle attached is a GM Variant.
        private static bool IsGMMake(this string Make)
        {
            if (string.IsNullOrEmpty(Make)) { return false; }
            bool IsGM =
                (Make.ToUpper() == "BUICK") ||
                (Make.ToUpper() == "CHEVROLET") ||
                (Make.ToUpper() == "CADILLAC") ||
                (Make.ToUpper() == "DODGE") ||
                (Make.ToUpper() == "GMC");
            return IsGM;
        }

        // Checks if the vehicle attached is a Ford Variant.
        private static bool IsFordMake(this string Make)
        {
            if (string.IsNullOrEmpty(Make)) { return false; }
            bool IsFord =
                (Make.ToUpper() == "FORD") ||
                (Make.ToUpper() == "LINCOLN") ||
                (Make.ToUpper() == "MERCURY");
            return IsFord;
        }

        // Checks if the vehicle attached is a Honda Variant.
        private static bool IsHondaMake(this string Make)
        {
            if (string.IsNullOrEmpty(Make)) { return false; }
            bool IsHonda =
                (Make.ToUpper() == "HONDA") ||
                (Make.ToUpper() == "ACURA");
            return IsHonda;
        }

        // Checks if the vehicle attached is a Nissan Variant.
        private static bool IsNissanMake(this string Make)
        {
            if (string.IsNullOrEmpty(Make)) { return false; }
            bool IsNissan =
                (Make.ToUpper() == "NISSAN") ||
                (Make.ToUpper() == "INFINITI");
            return IsNissan;
        }
        #endregion

        /// <summary>
        /// Checks if the vehicle attached is a FDRS enabled vehicle.
        /// </summary>
        /// <param name="VehicleInfo"></param>
        /// <param name="Model"></param>
        /// <param name="VehicleYear"></param>
        /// <returns></returns>
        public static bool IsFDRSVehicle(this VehicleScan VehicleInfo, string Model, int VehicleYear)
        {
            // If it's not a ford.
            if (!VehicleInfo.HasFlag(VehicleScan.FDRS) && !VehicleInfo.HasFlag(VehicleScan.FJDS))
                return false;

            switch (Model.ToUpper())
            {
                case "BRONCO SPORT":
                case "BRONCO":
                case "EDGE":
                case "F-150":
                case "MUSTANG":
                case "MUSTANG MACH-E":
                case "NAUTILUS":
                case "SUPERDUTY":
                    return (VehicleYear >= 2021);

                case "AVIATOR":
                case "CORSAIR":
                case "ESCAPE":
                case "EXPLORER":
                case "TRANSIT":
                    return (VehicleYear >= 2020);

                case "RANGER":
                case "TRANSIT CONNECT":
                    return (VehicleYear >= 2019);

                case "EXPEDITION":
                case "ECOSPORT":
                case "NAVIGATOR":
                    return (VehicleYear >= 2018);

                // If we're here and have nothing to return:
                default: return false;
            }
        }
        /// <summary>
        /// Gets the OE Scan app for a provided vehicle info enum
        /// </summary>
        /// <param name="VehicleInfo"></param>
        /// <returns>Type of oe scan app</returns>
        public static VehicleScan ScanApp(this VehicleScan VehicleInfo, int Year = 0, string Model = "")
        {
            // Find the make type apps
            if (IsToyotaMake(VehicleInfo.ToString())) { return VehicleScan.TIS; }
            if (IsGMMake(VehicleInfo.ToString())) { return VehicleScan.GDS2; }
            if (IsNissanMake(VehicleInfo.ToString())) { return VehicleScan.C3P; }
            if (IsHondaMake(VehicleInfo.ToString())) { return VehicleScan.HDS; }
            if (IsFordMake(VehicleInfo.ToString()))
                return IsFDRSVehicle(VehicleInfo, Model, Year) ? VehicleScan.FDRS : VehicleScan.FJDS;
            return VehicleScan.INVALID;
        }
    }
}
