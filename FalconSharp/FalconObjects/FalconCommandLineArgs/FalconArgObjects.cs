﻿using CommandLine;

namespace FalconObjects.FalconCommandLineArgs
{
    /// <summary>
    /// Base Options setup for the args objects
    /// </summary>
    public class FalconOptionsBase
    {
        [Option("force-debugging-session", Required = false, HelpText = "Forces a debugging session to startup.")]
        public bool ForceDebuggingSession { get; set; }

        /// <summary>
        /// Vehicle configuration information
        /// </summary>
        public struct VehicleInfo
        {
            // Values
            public int VehicleYear;
            public string VehicleMake;
            public string VehicleModel;
            public string VehicleVin;

            // YMM Value
            public string YMMString => $"{this.VehicleYear}_{this.VehicleMake}_{this.VehicleModel}".ToUpper();
        }

        /// <summary>
        /// Struct for debugging toggles.
        /// </summary>
        public struct DebuggingInfo
        {
            public bool BuildJsonTemplates;
            public bool ShowInjectionWindow;
            public bool SuperTalonLogging;
        }
    }


    /// <summary>
    /// Main Scan Session Startup
    /// </summary>
    [Verb("FalconSession", HelpText = "Vehicle Configuration Information and Scan Type")]
    public class FalconSessionOptions : FalconOptionsBase
    {
        [Option("year", Required = true, HelpText = "Year Of Vehicle")]
        public int VehicleYear
        {
            get => this.VehicleConfiguration.VehicleYear;
            set => this.VehicleConfiguration.VehicleYear = value;
        }
        [Option("make", Required = true, HelpText = "Make Of Vehicle")]
        public string VehicleMake
        {
            get => this.VehicleConfiguration.VehicleMake;
            set => this.VehicleConfiguration.VehicleMake = value;
        }
        [Option("model", Required = true, HelpText = "Model Of Vehicle")]
        public string VehicleModel
        {
            get => this.VehicleConfiguration.VehicleModel;
            set => this.VehicleConfiguration.VehicleModel = value;
        }
        [Option("vin", Required = true, HelpText = "VIN Number Of Vehicle")]
        public string VehicleVin
        {
            get => this.VehicleConfiguration.VehicleVin;
            set => this.VehicleConfiguration.VehicleVin = value;
        }

        // ----------------------------------------------------------------------------------------------

        // Vehicle Configuration
        public VehicleInfo VehicleConfiguration;

        /// <summary>
        /// Build new vehicle configuration object
        /// </summary>
        public FalconSessionOptions()
        {
            // Build new vehicle configuration
            this.VehicleConfiguration = new VehicleInfo();
        }
    }
    /// <summary>
    /// Falcon Session with debugging configuration
    /// </summary>
    [Verb("FalconDebugSession", HelpText = "Diagnostic Options/Debug Options")]
    public class FalconDebuggingOptions : FalconSessionOptions
    {
        [Option("build-json-templates", Required = false, HelpText = "Generate new default JSON objects for scan info")]
        public bool BuildJsonTemplates
        {
            get => this.DebugConfiguration.BuildJsonTemplates;
            set => this.DebugConfiguration.BuildJsonTemplates = value;
        }
        [Option("show-injection-window", Required = false, HelpText = "Generate new default JSON objects for scan info")]
        public bool ShowInjectionWindow
        {
            get => this.DebugConfiguration.ShowInjectionWindow;
            set => this.DebugConfiguration.ShowInjectionWindow = value;
        }
        [Option("super-talon-logging", Required = false, HelpText = "Generate new default JSON objects for scan info")]
        public bool SuperTalonLogging
        {
            get => this.DebugConfiguration.SuperTalonLogging;
            set => this.DebugConfiguration.SuperTalonLogging = value;
        }

        // ----------------------------------------------------------------------------------------------

        // Debug Configuration
        public DebuggingInfo DebugConfiguration;

        /// <summary>
        /// Build new debugging configuration object
        /// </summary>
        public FalconDebuggingOptions()
        {
            // Build new Debug object
            this.DebugConfiguration = new DebuggingInfo();
        }
    }
}
