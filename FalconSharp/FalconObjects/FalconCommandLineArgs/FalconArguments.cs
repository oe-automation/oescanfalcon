﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using ERSConstructor;
using ERSConstructor.DynamicERS;
using Newtonsoft.Json;

namespace FalconObjects.FalconCommandLineArgs
{
    /// <summary>
    /// Class which holds arguments for the CLI
    /// </summary>
    public class FalconArguments
    {
        // Parsed Status
        public int ParseStatusCode;
        public bool ParsePassed = false;
        private DynamicERSLogger Logger;

        // Logger
        public Type ArgsObjectType;
        public dynamic FalconArgsObject;
 
        /// <summary>
        /// Builds a new FalconArgument set
        /// </summary>
        /// <param name="CLIArgs">Args to parse out.</param>
        public FalconArguments(string[] CLIArgs)
        {
            // Get Logger, setup arguments.
            this.Logger = LoggingHelpers.MainLogger;
            this.ParseStatusCode = Parser.Default.ParseArguments<FalconOptionsBase,FalconSessionOptions,FalconDebuggingOptions>(CLIArgs)
                .MapResult(
                    (FalconOptionsBase BaseArgs) => SetOptionsObject(BaseArgs),
                    (FalconSessionOptions VehicleOpts) => SetOptionsObject(VehicleOpts),
                    (FalconDebuggingOptions DebugOpts) => SetOptionsObject(DebugOpts),
                    ErrorsFound => -1);

            // Set parsed pass status
            this.ParsePassed = this.ParseStatusCode != -1;
        }


        /// <summary>
        /// Parse arg set and print out their values to the console.
        /// </summary>
        /// <param name="Options">Options to parse.</param>
        /// <returns>An int based status code.</returns>
        private int SetOptionsObject(object Options)
        {
            // Get Json Version and print. Type cast and store.
            Type OptionType = Options.GetType();
            string JsonObjects = JsonConvert.SerializeObject(Options, Formatting.Indented);
            Logger.WriteLogEntry($"GENERATED NEW SETTINGS ENTRY CAST AS: {OptionType.Name}", LogEntryType.LOG_DEBUG);
            Logger.WriteLogEntry($"ARG OBJECT CONTENTS:\n{JsonObjects}", LogEntryType.LOG_TRACE);

            // Unwrap and save values
            this.ArgsObjectType = Options.GetType();
            if (OptionType == typeof(FalconOptionsBase)) { this.FalconArgsObject = (FalconOptionsBase)Options; return -1; }
            if (OptionType == typeof(FalconSessionOptions)) { this.FalconArgsObject = (FalconSessionOptions)Options; return 0; }
            if (OptionType == typeof(FalconDebuggingOptions)) { this.FalconArgsObject = (FalconDebuggingOptions)Options; return 0; }

            // Failed to convert/save
            Logger.WriteLogEntry($"FAILED TO STORE SETTINGS AND ARGUMENTS FOR TYPE {OptionType.Name}!", LogEntryType.LOG_ERROR);
            return -1;
        }
    }
}