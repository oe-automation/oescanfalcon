﻿namespace FalconObjects.FalconParameterObjects
{
    /// <summary>
    /// AWS Configuration setup for the Falcon Scan
    /// </summary>
    public class FalconAWSConfiguration
    {
        public string S3BucketName { get; set; }
        public string S3AccessKey { get; set; }
        public string S3SecretKey { get; set; }
    }
}
