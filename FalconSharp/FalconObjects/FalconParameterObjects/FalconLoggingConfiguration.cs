﻿using System;
using ERSConstructor.DynamicERS;

namespace FalconObjects.FalconParameterObjects
{
    /// <summary>
    /// Class dedicated to logging configuration for Falcon.
    /// </summary>
    public class FalconLoggingConfiguration
    {
        public LogEntryType MinLogging { get; set; }
        public string BaseOutput { get; set; }
        public string CustomLoggingName { get; set; }
        public string ConnectionString { get; set; }
    }
}
