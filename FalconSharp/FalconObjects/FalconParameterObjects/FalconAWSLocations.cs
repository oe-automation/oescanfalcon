﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FalconObjects.FalconParameterObjects
{
    /// <summary>
    /// AWS Locations for Falcon Updater
    /// </summary>
    public class FalconAWSLocations
    {
        public string IronPythonLocalBase { get; set; }
        public string IronPythonScripts { get; set; }
        public string IronPythonStdLib { get; set; }
    }
}
